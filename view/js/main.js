function amigable1(url) {
    var link="";
    url = url.replace("?", "");
    url = url.split("&");

    for (var i=0;i<url.length;i++) {
        var aux = url[i].split("=");
        link +=  "/"+aux[1];
    }
    return link;
}

function amigable(url) {
    var count = 0;
    var link="";
    url = url.replace("?", "");
    url = url.split("&");

    for (var i=0;i<url.length;i++) {
        count++;
        var aux = url[i].split("=");
        if(count == 2){
            link +=  "/"+aux[1]+"/";
        }else {
            link +=  "/"+aux[1];
        }
    }
    return "/workspace/FW_PHP_MVC_OO_JS_JQuery" + link;
}
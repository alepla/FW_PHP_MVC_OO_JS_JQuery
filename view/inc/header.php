<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pla's Music | <?php if($_GET['module']){echo $_GET['module'];}else{echo "homepage";}?></title>
	
	<link href="<?php echo CSS_PATH ?>bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo CSS_PATH ?>style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo CSS_PATH ?>font-awesome.css" rel="stylesheet"> 
	<link href="<?php echo CSS_PATH ?>toastr.css" rel="stylesheet"> 

	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo LOGIN_JS_PATH ?>init.js"></script>
	<script type="text/javascript" src="<?php echo JS_PATH ?>toastr.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_PATH ?>cookies.js"></script>
	<script src='<?php echo LOGIN_JS_PATH ?>logout.js'></script>
	<script type="text/javascript" src="<?php echo JS_PATH ?>main.js"></script>

</head>
<body>

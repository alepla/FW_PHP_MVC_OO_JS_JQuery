<!-- banner -->
<div class="banner">
    <div class="banner-dott">
        <!-- Top-Bar -->
                <div class="top">
                    <div class="container">
                        
                            <div class="col-md-6 top-left">
                                <ul>
                                    <li><i class="fa fa-mobile" aria-hidden="true"></i> +021 365 777</li>
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> 132 New Lenox, 868 1st Avenue </li>
                                </ul>
                            </div>
                            <div class="col-md-6 top-middle">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        
                    </div>
                </div>
                <div class="top-bar">
                <div class="container">
                    <div class="header">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header navbar-left">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <h1><a class="navbar-brand" href="<?php amigable('?module=main&function=load_main'); ?>">Pla's Music</span></a></h1>
                            </div>
                            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                                <nav class="cl-effect-15" id="cl-effect-15">
                                    <ul class="nav navbar-nav">
                                        <li class=<?php if($_GET['module']== "main"){ echo "active";}else{ echo "";}?>><a href="<?php amigable('?module=main&function=load_main'); ?>">Home</a></li>
                                        <!--<li id="LogMusic" class=<?php// if($_GET['module']== "albums"){ echo "active";}else{ echo "";}?>><a href="<?php// amigable('?module=albums&function=create_albums'); ?>">Music</a></li>-->
                                        <li class=<?php if($_GET['module']== "shop"){ echo "active";}else{ echo "";}?>><a href="<?php amigable('?module=shop&function=list_shop'); ?>">Shop</a></li>
                                        <li class=<?php if($_GET['module']== "contact"){ echo "active";}else{ echo "";}?>><a href="<?php amigable('?module=contact&function=list_contact'); ?>">Contact us</a></li>
                                        <li id="LogUser"><a href="<?php amigable('?module=login&function=load_login'); ?>">Sign In</a></li>
                                    </ul>  
                                </nav>
                            </div>
                        </nav>
                </div>
            </div>
        </div>
        <div class="w3-banner">
            <div id="typer"></div>
                <p>Where words fail, music speaks.</p>
        </div>
    </div>
</div>
<br/>
<div class="col-sm-6">
    <h4><?php
        if (!isset($_GET['module'])) {
            echo "main";
        } else if (isset($_GET['module']) && !isset($_GET['view'])) {
            echo "Pla Music /<a class='breakcrount' href='../../" . $_GET['module'] . "/" . $_GET['function'] . "/'> " . $_GET['module'] . "</a>";
        }else{
            echo "Pla Music /<a class='breakcrount' href='../../" . $_GET['module'] . "/" . $_GET['function'] . "/".$_GET['aux']."/'> " . $_GET['module'] . "</a>";
        }
        ?>
    </h4>
</div>
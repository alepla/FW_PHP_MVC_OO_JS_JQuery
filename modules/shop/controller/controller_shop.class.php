<?php

class controller_shop {
    function __construct() {
        $_SESSION['module'] = "shop";
    }

    function list_shop() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/shop/view/', 'list_shop.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }
///////////////////////////////////////////////DETAILS
    function idProduct() {
        if ($_POST["idProduct"]) {
            $id = $_POST["idProduct"];
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "details_shop",$id);
            if ($arrValue) {
                $jsondata["album"] = $arrValue[0];
                echo json_encode($jsondata); 
                exit;
            }else {
                echo json_encode("error");
                exit;
            }
        } 
    }
    function add() {
        if ($_POST["add"]) {
            $id = $_POST["add"];
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "add_one",$id);
            echo json_encode($arrValue);
            exit;
        }         
    }

//////////////////////////////////////////////LIST
    function prods() {
        if (isset($_POST["prods"])) {
            $item_per_page = 4;
            $page_number = $_POST["prods"];
            $position = (($page_number - 1) * $item_per_page);
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "list_shop", $position);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }        
    }

////////////////////////////////////////////LIST+KEY
    function prod() {
        if (isset($_POST["prod"])) {
            $item_per_page = 4;
            $page_number = $_POST["prod"];
            $key = $_POST['key'];
            $position = (($page_number - 1) * $item_per_page);
            $arrArgument = array(
                "valor" => $key,
                "position" => $position
            );
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "select_like_limit_products", $arrArgument);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }
    }

/////////////////////////////////////////////BOOT_PAGE
    function total_pages() {
        if (isset($_POST["total_pages"]) && ($_POST["total_pages"] == true)) {
            if (isset($_POST["keyword"])) {
                $count = $_POST["keyword"];
            } else {
                $count = '';
            }
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "count_like_shop", $count);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }
    }
///////////////////////////////////////////AUTOCOMPLETE

    function autocomplete() {
        if (isset($_POST["autocomplete"]) && ($_POST["autocomplete"] == true)) {
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "autocomplete_shop");
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }        
    }

//////////////////////////////////////////COUNT

    function count() {
        if (isset($_POST["count"])) {
            $count = $_POST["count"];
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "count_like_shop", $count);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }        
    }

///////////////////////////////////////////SEARCH_ONE_ALBUM
    function albume_name() {
        if (isset($_POST["albume_name"])) {
            $album_name = $_POST["albume_name"];
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "albume_name_shop", $album_name);
            if ($arrValue) {
                $jsondata["album"] = $arrValue[0];
                echo json_encode($jsondata);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }        
    }

////////////////////////////////////MAIN-SEARCH
    function sele_key() {
        session_start();
        if (isset($_POST["sele_key"]) && ($_POST["sele_key"] == true)) {
            if (isset($_SESSION['selected_key'])) {
                @$sel_key = $_SESSION['selected_key'];
                echo json_encode($sel_key);
                unset($_SESSION['selected_key']);
                $_SESSION = array();
                //session_destroy();
                exit;
            } else {
                echo json_encode("no");
                exit;
            }
        }
    }
}
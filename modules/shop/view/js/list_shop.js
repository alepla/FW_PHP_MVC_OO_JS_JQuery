$(document).ready(function () {

    if (getCookie("search")) {
        var keyword=getCookie("search");
        count(keyword);
        setCookie("search","",1);
    } else {
        search();
    }
    $.post(amigable("?module=shop&function=sele_key"), {'sele_key': true}, function (data, status) {
        var json = JSON.parse(data);
        if(json !== "no") {
            var keyword = json;
            var v_keyword = validate_search(keyword);
            if (v_keyword)
                setCookie("search", keyword, 1);
            location.reload(true);
        }
    });
    
    $("#search_prod").submit(function (e) {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);
        if (v_keyword)
            setCookie("search", keyword, 1);
        location.reload(true);
        e.preventDefault();
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);
        if (v_keyword)
            setCookie("search", keyword, 1);
        location.reload(true);
    });
    $.post(amigable("?module=shop&function=autocomplete"), {'autocomplete': true}, function (data, status) {
        var autocomplete_discs = JSON.parse(data);
        var suggestions = new Array();
        for (var i = 0; i < autocomplete_discs.length; i++) {
            suggestions.push(autocomplete_discs[i].discname);
        }
        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                var keyword = ui.item.label;
                count(keyword);
            }
        });
    });
    $('.imp_map_c').click(function () {
        window.location=amigable("?module=map&function=load_map");
    });
});

function count(keyword) {
    $.post(amigable("?module=shop&function=count"), {'count': keyword}, function (data, status) {
        var num = JSON.parse(data);
        var count_num = num[0].total;
        if (count_num == 0) {
            reset();
            $("#results").append("<h2>No one albume</h2>");
            $('.pagination').html('');
        }
        if (count_num == 1) {
            search_one_album(keyword);
        }
        if (count_num > 1) {
            search(keyword);
        }
    });
}

function search(keyword) {
    $.post(amigable("?module=shop&function=total_pages"), {'total_pages': true, 'keyword': keyword}, function (data, status) {
        var js = JSON.parse(data);
        var pages = 0;
        pages = parseFloat(js[0].total) / 4;
        var redo = Math.ceil(pages);
        if (!keyword){
            $.post(amigable("?module=shop&function=prods"), {'prods': 1}, function (data, status) {
                var json = JSON.parse(data);
                print_albumns(json);
            });
        }else{
            $.post(amigable("?module=shop&function=prod"), {'prod': 1, 'key': keyword}, function (data, status) {
                var json = JSON.parse(data);
                print_albumns(json);
            });
        }
        if (redo !== 0) {
            refresh();
            $(".pagination").bootpag({
                total: redo,
                page: 1,
                maxVisible: 3,
                next: 'Next Page',
                prev: 'Previous Page'
            }).on("page", function (e, num) {
                e.preventDefault();
                if (!keyword) {
                    $.post(amigable("?module=shop&function=prods"), {'prods': num}, function (data, status) {
                        var json = JSON.parse(data);
                        print_albumns(json);
                    });
                }else {
                    $.post(amigable("?module=shop&function=prod"), {'prod': num, 'key': keyword}, function (data, status) {
                        var json = JSON.parse(data);
                        print_albumns(json);
                    });
                }
                reset();
            });
        }else {
            console.log("No albums");
            reset();
        }
        reset();
    });
}

function search_one_album(keyword) {
    $.post(amigable("?module=shop&function=albume_name"), {'albume_name': keyword}, function (data, status) {
        var albume_name = JSON.parse(data);
        reset();
        $('.pagination').html('');
        var album = albume_name.album;
        var ro = "<div class='w3_two_grid_left1 mod'>" +
                        "<div class='col-xs-3 w3_two_grid_left_grid img_mod'><img src=" + album.discpic + "></div>" +
                        "<h4 class='name_prod'>" + album.discname + "</h4>" +
                        "<h4 class='studies_prod'>" + album.artist + "</h4>" +
                        "<h3 class='stock'>" + album.discprice + ".00$</h3>" +
                        "<p class='stock'>" + album.discdesc + "</p>" +
                        "<div class='clearfix'></div>" +
                      "</div>";
        $("#results").append(ro);
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return 0;
}

function reset() {
    $("#results").empty();
}

function refresh() {
    $('.pagination').html = '';
    $('.pagination').val = '';
}

function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .,]*$/;
        return regexp.test(search_value);
    }
    return false;
}

function print_albumns(data) {
    var json = data;
    for(var i in json){
        var row = "<div class='lis'>" +
                    "<div class='lis2'>" +
                        "<a data-target='#exampleModal' data-toggle='modal' id=" + json[i].discid + " class='click_img_list'>" +
                        "<img class='img_lis' src=" + json[i].discpic + "></img></a>" +
                        "<p>" + json[i].discname + "</p>" +
                        "<p>" + json[i].discprice + ".00$</p>" +
                    "</div>" +
                  "</div>";
        $("#results").append(row);
    }
}
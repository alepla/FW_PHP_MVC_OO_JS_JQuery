<?php

class shop_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = shop_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_shop_BLL($position) {
        return $this->dao->list_shop_DAO($this->db, $position);
    }

    public function details_shop_BLL($id) {
        return $this->dao->details_shop_DAO($this->db,$id);
    }

    public function count_shop_BLL() {
        return $this->dao->count_shop_DAO($this->db);
    }

    public function autocomplete_shop_BLL() {
        return $this->dao->autocomplete_shop_DAO($this->db);
    }

    public function count_like_shop_BLL($count) {
        return $this->dao->count_like_shop_DAO($this->db, $count);
    }

    public function albume_name_shop_BLL($album_name) {
        return $this->dao->albume_name_shop_DAO($this->db, $album_name);
    }

    public function select_like_limit_products_BLL($arrArgument) {
        return $this->dao->select_like_limit_products_DAO($this->db, $arrArgument);
    }

    public function genre_main_BLL($gen) {
        return $this->dao->genre_main_DAO($this->db, $gen);
    }

    public function add_one_BLL($id) {
        return $this->dao->add_one_DAO($this->db, $id);
    }
    
}


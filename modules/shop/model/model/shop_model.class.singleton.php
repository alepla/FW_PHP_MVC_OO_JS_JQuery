<?php

class shop_model {
    private $bll;
    static $_instance;
    
    private function __construct() {
        $this->bll = shop_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }
    
    public function list_shop($position) {
        return $this->bll->list_shop_BLL($position);
    }
    
    public function details_shop($id) {
        return $this->bll->details_shop_BLL($id);
    }

    public function count_shop() {
        return $this->bll->count_shop_BLL();
    }

    public function autocomplete_shop() {
        return $this->bll->autocomplete_shop_BLL();
    }

    public function count_like_shop($count) {
        return $this->bll->count_like_shop_BLL($count);
    }

    public function albume_name_shop($album_name) {
        return $this->bll->albume_name_shop_BLL($album_name);
    }

    public function select_like_limit_products($arrArgument) {
        return $this->bll->select_like_limit_products_BLL($arrArgument);
    }

    public function genre_main($gen) {
        return $this->bll->genre_main_BLL($gen);
    }

    public function add_one($id) {
        return $this->bll->add_one_BLL($id);
    }
}

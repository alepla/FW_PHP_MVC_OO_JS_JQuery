<?php
class shop_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }
    
    public function list_shop_DAO($db,$position) {
        $sql = "SELECT discpic, discname, discprice, discid FROM albums LIMIT $position,4";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }
    
    public function details_shop_DAO($db,$id) {
        $sql = "SELECT discpic, discname, discprice, discid, discdesc, artist FROM albums WHERE discid=".$id;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function count_shop_DAO($db) {
        $sql = "SELECT COUNT(*) AS tot FROM albums";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function autocomplete_shop_DAO($db) {
        $sql = "SELECT discname FROM albums ORDER BY discname";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function count_like_shop_DAO($db, $count) {
        $sql = "SELECT COUNT(*) as total FROM albums WHERE discname LIKE '%" . $count . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function albume_name_shop_DAO($db, $album_name) {
        $sql = "SELECT DISTINCT * FROM albums WHERE discname LIKE '%" . $album_name . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function select_like_limit_products_DAO($db, $arrArgument) {
        $sql="SELECT DISTINCT * FROM albums WHERE discname LIKE '%". $arrArgument['valor']. "%' ORDER BY discname ASC LIMIT ". $arrArgument['position']." ,4";
        $stmt=$db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function genre_main_DAO($db,$gen) {
        $sql = "SELECT discpic,discid,discname,discprice,artist FROM albums WHERE genre = '$gen'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function add_one_DAO($db, $id) {
        $sql = "UPDATE albums SET n_views = (n_views + 1) WHERE discid = '$id'";
        $stmt = $db->ejecutar($sql);
        return "good";
        
    }
    
}

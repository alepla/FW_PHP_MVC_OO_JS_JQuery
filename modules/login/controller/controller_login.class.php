<?php

class controller_login {
    function __construct() {
    	require_once(UTILS_LOGIN . "functions.inc.php");
        require_once(UTILS . "upload.php");
        $_SESSION['module'] = "login";
    }

    function load_login() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'login.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_register() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'register.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_recover_pass() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'recover_pass.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_verify() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'login.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_503(){
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        require_once(VIEW_PATH_INC . "503.php");

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_activate() {
        require_once(VIEW_PATH_INC . "header.php");

        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'modify_pass.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_profile(){
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

         loadView('modules/login/view/', 'profile.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }


    public function register_user() {
        $jsondata = array();
        $userJSON = json_decode($_POST['register_data_json'], true);
        $result = validate_userPHP($userJSON);

        if ($result['result']) {
            $avatar = get_gravatar($result['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
            $arrArgument = array(
                'user' => $result['data']['user'],
                'email' => $result['data']['email'],
                'pass' => password_hash($result['data']['pass'], PASSWORD_BCRYPT),
                'type' => $result['data']['type'],
                'avatar' => $avatar,
                'token' => ""
            );

            $arrValue = false;
            $arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "alta_user", $arrArgument);

            if ($arrValue) {
                sendtoken($arrArgument, "alta");
                $jsondata["success"] = true;
                $jsondata['datos'] = $arrArgument;
                echo json_encode($jsondata);
                exit();
            }else {
                $jsondata["success"] = false;
                $jsondata['error'] = $result['error'];
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit();
            }
        }else {
            $jsondata["success"] = false;
            $jsondata['error'] = $result['error'];
            header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit();
        }
    }

    function verify() {

        if (substr($_GET['param'], 0, 3) == "Ver") {
            $argument = array(
                'token' => $_GET['param']
            );

            $value = false;
            $value = loadModel(MODEL_LOGIN, "login_model", "verify_account", $argument);

            if ($value) {
                echo '<script>window.location="../../load_verify/";</script>';
            } else {
                echo '<script>window.location="../../load_503/";</script>';
            }
        }
    }

    public function access() {
        $jsondata = array();
        $info_user = json_decode($_POST['login_json'], true);
        $login_info = array(
            'username' => $info_user['user']
        );

        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "access_count", $login_info);
        $arrValue = password_verify($info_user['pass'], $arrValue[0]['password']);
        if ($arrValue) {
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "val_count", $login_info);
            if ($arrValue[0]['active'] == 1) {
                $info = loadModel(MODEL_LOGIN, "login_model", "select_info_user", $login_info);
                if($info){
                    $jsondata["success"] = true;
                    $jsondata['data'] = $info;
                    echo json_encode($jsondata);
                    exit();
                }else {
                    $jsondata["success"] = false;
                    $jsondata['error'] = $result['error'];
                    echo json_encode($jsondata);
                    exit();  
                }
            }else {
                echo json_encode("error1");
                exit();
            }
        }else {
            echo json_encode("error");
            exit();
        }
    }

    function recover_pass() {
        $email = $_POST['email'];
        $_SESSION['email'] = $email;
        $val = validatemail($email);

        if($val) {
            $arrValue = false;
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email", $val);
            if ($arrValue){
                $token = "Cha" . md5(uniqid(rand(), true));
                $arrArgument = array(
                    'token' => $token,
                    'email' => $email
                );
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "change_token", $arrArgument);
                if($arrValue){
                    sendtoken($arrArgument, "mod");
                    echo json_encode("good");
                }else{
                    echo json_encode("error2");
                }
            }else {
                echo json_encode("error1");
            }
        }else {
            echo json_encode("error");
        }
    }

    function activate() {
        if (substr($_GET['param'], 0, 3) == "Cha") {
            $tok = $_GET['param'];
            $_SESSION['token'] = $tok;
            echo '<script>window.location="../../load_activate/";</script>';
        } else {
            echo '<script>window.location="../../load_503/";</script>';
        }
    }

    function change_pass() {
        $tok = $_SESSION['token'];
        $val = $_SESSION['email'];
        $new_pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);

        $new_token = "Ver" . md5(uniqid(rand(), true));
        $arrArgument = array(
            'token' => $tok,
            'new_pass' => $new_pass,
            'new_token' => $new_token
        );

        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "change_password", $arrArgument);

        if ($arrValue) {
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email", $val);
            if (substr($arrValue[0]['token'], 0, 3) == "Ver") {
                echo json_encode("error1");
            }else {
                echo json_encode("good");
                $arrvalue = loadModel(MODEL_LOGIN, "login_model", "change_token_2", $arrArgument);
            }
        }else {
            echo json_encode("error");
        }
    }

    function list_profile() {
        $username = $_POST['username'];

        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "list_user_profile", $username);

        if($arrValue) {
            echo json_encode($arrValue);
        }else {
            echo json_encode("error");
        }
    }

    function upload() {
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)){
            $profile_image = upload_files();
            $_SESSION['profile_image'] = $profile_image;
        }
    }

    function delete(){
        $_SESSION['profile_image'] = array();
        $result = remove_files();
        if ($result === true) {
            echo json_encode(array("res" => true));
        } else {
            echo json_encode(array("res" => false));
        }
    }

    function update_info_prof(){
        $profile_data_json = json_decode($_POST['profile_data_json'], true);
        if (empty($_SESSION['profile_image'])){
            $img_profile = array('result' => true, 'error' => "", "data" => $profile_data_json['avatar']);
        }else{
            $img_profile = $_SESSION['profile_image'];
        }
        if($profile_data_json['birthday'] != ""){
            $age = validate_age($profile_data_json['birthday']);
        }else{
            $age = 7;
        }
        if ($age < 6){
            echo json_encode("error1");
        }else {
            if ($img_profile['result']){
                $arrArgument = array(
                    'user' => $profile_data_json['user'],
                    'name' => $profile_data_json['name'],
                    'lastname' => $profile_data_json['lastname'],
                    'city' => $profile_data_json['city'],
                    'birthday' => $profile_data_json['birthday'],
                    'img_profile' => $img_profile['data']
                );
                $arrValue = false;
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_user_profile", $arrArgument);
                if ($arrValue){
                    session_unset($_SESSION['profile_image']);
                    echo json_encode("good");
                }else {
                    echo json_encode("error");
                }
            }else {
                $jsondata['success'] = false;
                $jsondata['error'] = $result['error'];
                $jsondata['img_profile'] = $img_profile['error'];
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
            }
        }
    }

    function social(){
        $social_info = json_decode($_POST['social_json'], true);
        if($social_info['email'] == null){
            echo json_encode("email_no");
        }else {
            $arrArgument = array(
                'user' => $social_info['user'],
                'name' => $social_info['name'],
                'email' => $social_info['email'],
                'type' => "client",
                'avatar' => $social_info['avatar']
            );
            $arrValue = false;
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "search_social", $arrArgument);
            if($arrValue){
                $arrValue = false;
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_social", $arrArgument);
                if($arrValue){
                    echo json_encode($arrValue);
                }else {
                    echo json_encode("error");
                }
            }else {
                $val = email_rep($social_info['email']);
                if ($val){
                    echo json_encode("email_rep");
                }else{ 
                    $arrValue = false;
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "insert_social", $arrArgument);
                    if($arrValue){
                        $arrValue = false;
                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_social", $arrArgument);
                        if($arrValue){
                            echo json_encode($arrValue);
                        }else {
                            echo json_encode("error");
                        }
                    }else {
                        echo json_encode("error1");
                    }
                }
            }
        }
    }
}
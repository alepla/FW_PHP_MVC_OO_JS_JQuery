<?php

class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_username($username) {
        return $this->bll->select_username_BLL($username);
    }

    public function alta_user($arrArgument) {
        return $this->bll->alta_user_BLL($arrArgument);
    }

    public function verify_account($argument) {
        return $this->bll->verify_account_BLL($argument);
    }

    public function access_count($login_info) {
        return $this->bll->access_count_BLL($login_info);
    }

    public function val_count($login_info) {
        return $this->bll->val_count_BLL($login_info);
    }

    public function select_info_user($login_info) {
        return $this->bll->select_info_user_BLL($login_info);
    }

    public function validate_email($val) {
        return $this->bll->validate_email_BLL($val);
    }

    public function change_token($arrArgument) {
        return $this->bll->change_token_BLL($arrArgument);
    }

    public function change_password($arrArgument) {
        return $this->bll->change_password_BLL($arrArgument);
    }

    public function list_user_profile($username) {
        return $this->bll->list_user_profile_BLL($username);
    }

    public function change_token_2($arrArgument) {
        return $this->bll->change_token_2_BLL($arrArgument);
    }

    public function update_user_profile($arrArgument) {
        return $this->bll->update_user_profile_BLL($arrArgument);
    }

    public function search_social($arrArgument) {
        return $this->bll->search_social_BLL($arrArgument);
    }

    public function select_social($arrArgument) {
        return $this->bll->select_social_BLL($arrArgument);
    }

    public function insert_social($arrArgument) {
        return $this->bll->insert_social_BLL($arrArgument);
    }
}

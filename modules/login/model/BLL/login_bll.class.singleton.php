<?php

class login_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_username_BLL($username) {
        return $this->dao->select_username_DAO($this->db, $username);
    }

    public function alta_user_BLL($arrArgument) {
        return $this->dao->alta_user_DAO($this->db, $arrArgument);
    }

    public function verify_account_BLL($argument) {
        return $this->dao->verify_account_DAO($this->db, $argument);
    }

    public function access_count_BLL($login_info) {
        return $this->dao->access_count_DAO($this->db, $login_info);
    }

    public function val_count_BLL($login_info) {
        return $this->dao->val_count_DAO($this->db, $login_info);
    }

    public function select_info_user_BLL($login_info) {
        return $this->dao->select_info_user_DAO($this->db, $login_info);
    }

    public function validate_email_BLL($val) {
        return $this->dao->validate_email_DAO($this->db, $val);
    }

    public function change_token_BLL($arrArgument) {
        return $this->dao->change_token_DAO($this->db, $arrArgument);
    }

    public function change_password_BLL($arrArgument) {
        return $this->dao->change_password_DAO($this->db, $arrArgument);
    }

    public function list_user_profile_BLL($username) {
        return $this->dao->list_user_profile_DAO($this->db, $username);
    }
    
    public function change_token_2_BLL($arrArgument) {
        return $this->dao->change_token_2_DAO($this->db, $arrArgument);
    }

    public function update_user_profile_BLL($arrArgument) {
        return $this->dao->update_user_profile_DAO($this->db, $arrArgument);
    }

    public function search_social_BLL($arrArgument) {
        return $this->dao->search_social_DAO($this->db, $arrArgument);
    }

    public function select_social_BLL($arrArgument) {
        return $this->dao->select_social_DAO($this->db, $arrArgument);
    }

    public function insert_social_BLL($arrArgument) {
        return $this->dao->insert_social_DAO($this->db, $arrArgument);
    }
}
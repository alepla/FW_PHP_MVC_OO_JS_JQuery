<?php

class login_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_username_DAO($db, $username) {
        $sql = "SELECT * FROM users WHERE user='".$username."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }  

    public function alta_user_DAO($db, $arrArgument) {
        $username = $arrArgument['user'];
        $email = $arrArgument['email'];
        $pass = $arrArgument['pass'];
        $type = $arrArgument['type'];
        $avatar = $arrArgument['avatar'];
        $token = $arrArgument['token'];

        $sql = "INSERT INTO users (user, email, name, lastname,"
                . " password, date_birthday, type, city, avatar, active, token) VALUES ('$username', '$email',"
                . " '', '', '$pass', '', '$type', '', '$avatar', '', '$token')";

        return $db->ejecutar($sql);
        
    }

    public function verify_account_DAO($db, $argument) {
        $token = $argument['token'];

        $sql = "UPDATE users SET active = 1 WHERE token='".$token."'";

        return $db->ejecutar($sql);
        
    }

    public function access_count_DAO($db, $login_info) {
        $username = $login_info['username'];

        $sql = "SELECT password FROM users WHERE user='".$username."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }  

    public function val_count_DAO($db, $login_info) {
        $username = $login_info['username'];

        $sql = "SELECT active FROM users WHERE user='".$username."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }  

    public function select_info_user_DAO($db, $login_info) {
        $username = $login_info['username'];

        $sql = "SELECT * FROM users WHERE user='".$username."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }  

    public function validate_email_DAO($db, $val) {

        $sql = "SELECT token FROM users WHERE email='".$val."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function change_token_DAO($db, $arrArgument) {
        $token = $arrArgument['token'];
        $email = $arrArgument['email'];

        $sql = "UPDATE users SET token = '".$token."' WHERE email = '".$email."'";

        return $db->ejecutar($sql);
        
    }  

    public function change_password_DAO($db, $arrArgument) {
        $token = $arrArgument['token'];
        $new_token = $arrArgument['new_token'];
        $pass = $arrArgument['new_pass'];

        $sql = "UPDATE users SET password = '".$pass."' WHERE token = '".$token."'";

        return $db->ejecutar($sql);
        
    }

    public function list_user_profile_DAO($db, $username) {

        $sql = "SELECT * FROM users WHERE user='".$username."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function change_token_2_DAO($db, $arrArgument) {
        $token = $arrArgument['token'];
        $new_token = $arrArgument['new_token'];

        $sql = "UPDATE users SET token = '".$new_token."' WHERE token = '".$token."'";

        return $db->ejecutar($sql);
        
    }

    public function update_user_profile_DAO($db, $arrArgument) {
        $user = $arrArgument['user'];
        $name = $arrArgument['name'];
        $lastname = $arrArgument['lastname'];
        $city = $arrArgument['city'];
        $birthday = $arrArgument['birthday'];
        $img = $arrArgument['img_profile'];

        $sql = "UPDATE users SET name='".$name."', lastname='".$lastname."', city='".$city."', date_birthday='".$birthday."', avatar='".$img."' WHERE user='".$user."'";

        return $db->ejecutar($sql);
        
    }

    public function search_social_DAO($db, $arrArgument) {
        $user = $arrArgument['user'];

        $sql = "SELECT user FROM users WHERE user='".$user."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function select_social_DAO($db, $arrArgument) {
        $user = $arrArgument['user'];

        $sql = "SELECT * FROM users WHERE user='".$user."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function insert_social_DAO($db, $arrArgument) {
        $username = $arrArgument['user'];
        $email = $arrArgument['email'];
        $type = $arrArgument['type'];
        $avatar = $arrArgument['avatar'];

        $sql = "INSERT INTO users (user, email, name, lastname,"
                . " password, date_birthday, type, city, avatar, active, token) VALUES ('$username', '$email',"
                . " '', '', '', '', '$type', '', '$avatar', '1', '')";

        return $db->ejecutar($sql);
        
    }
}
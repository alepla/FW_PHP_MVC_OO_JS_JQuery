<?php
function validate_userPHP($value) {
    $error = array();
    $valid = true;
    $filter = array(
        'user' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9ü][a-zA-Z0-9ü_]{3,9}$/')
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validatemail'
        ),
        'pass' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,15}$/')
        ),
        'pass2' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,15}$/')
        )
    );
    $result = filter_var_array($value, $filter);

    $result['type'] = "client";
    $result['avatar'] = $value['avatar'];
    $result['token'] = $value['token'];

    if (username_rep($result['user'])){
        $error['user'] = "This username alrready exists";
        $valid = false;
    }

    if (email_rep($result['email'])){
        $error['email'] = "This email alrready exists";
        $valid = false;
    }

    if($result['user']==='Input a username'){
            $error['user']="Username must be 3 to 9 letters";
            $valid = false;
    }

    if ($result['email']==='Input a email'){
            $error['email']="Email must be 5 to 50 characters anb be correctly";
            $valid = false;    if ($result['pass'] !== $result['pass2']){
        $error['pass']="The first password and the next must be the same";
        $valid = false;
    }
    }



    if ($result != null && $result){
        if(!$result['user']){
            $error['user'] = "Username must be 3 to 9 letters";
            $valid = false;
        }

        if(!$result['email']){
            $error['email'] = "Email must be 5 to 50 characters anb be correctly";
            $valid = false;
        }

        if(!$result['pass']){
            $error['pass'] = "Password must be 8 to 15 characters and be like Password12.";
            $valid = false;
        }
        
    } else {
        $valid = false;
    };

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}

function validatemail($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}

function get_gravatar($email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array()) {
    $email = trim($email);
    $email = strtolower($email);
    $email_hash = md5($email);

    $url = "https://www.gravatar.com/avatar/" . $email_hash;
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    if ($img) {
        $url = '<img src="' . $url . '"';
        foreach ($atts as $key => $val)
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

function validate_age($date) {
    list($día_one, $mes_one, $anio_one) = split('/', $date);
    $dateOne = new DateTime($anio_one . "-" . $mes_one . "-" . $día_one);
    $now = new Datetime('today');
    $interval = $dateOne->diff($now);
    return $interval->y;
}

function sendtoken($arrArgument, $type) {
    $mail = array(
        'type' => $type,
        'token' => $arrArgument['token'],
        'email' => $arrArgument['email']
    );
    try {
        send_mail($mail);
        return true;
    } catch (Exception $e) {
        return false;
    }
}

function username_rep($username){
    $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_username", $username);
    return $arrValue;
}

function email_rep($val){
    $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email", $val);
    return $arrValue;
}

Dropzone.autoDiscover = false;
var avatar = "";
$(document).ready(function () {
	$("#prof_user_dropzone").hide();
   $(document).on("focus", "#profile_date_birthday", function(){
        $(this).datepicker({
        	changeMonth: true,
      		changeYear: true,
        	yearRange: "1940:2020",
        	dateFormat: 'dd/mm/yy',
        });
    });

    $("#dropzone").dropzone({
        url: amigable("?module=login&function=upload"),
        params:{'upload':true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                $("#prof_img_info").empty();
            });
        },
        complete: function (file) {
            /*if(file.status == "success"){
              alert("El archivo se ha subido correctamente: " + file.name);
            }*/
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: amigable("?module=login&function=delete"),
                data: {"filename":name,"delete":true},
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    } else {
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }
                }
            });
        }
    });

	var user = Tools.readCookie("user");
	if(user == null){
		window.location=amigable("?module=main&function=load_main");
	}
	user = user.split("|");
	var username = user[0];
	$.post(amigable("?module=login&function=list_profile"), {username: username}, function (response) {
		var json = JSON.parse(response);
		avatar = json[0]['avatar'];
		if(json != "error"){
			var info = "<img alt='' src=" + json[0]['avatar'] + ">";
    			$(".useravatar").append(info);
			var info = "<div class='w3_two_grid_left1 cesta'>" +
		                "<h4 class='padding_prof'><strong>USERNAME:</strong> " + json[0]['user'] + "</h4>" +
		                "<h4 class='padding_prof'><strong>EMAIL:</strong> " + json[0]['email'] + "</h4>" +
		                "<h4 class='padding_prof'><strong>NAME:</strong> " + json[0]['name'] + "</h4>" +
		                "<h4 class='padding_prof'><strong>LASTNAME:</strong> " + json[0]['lastname'] + "</h4>" +
		                "<h4 class='padding_prof'><strong>CITY:</strong> " + json[0]['city'] + "</h4>" +
		                "<h4 class='padding_prof'><strong>BIRTHDAY:</strong> " + json[0]['date_birthday'] + "</h4>" +
		                "<button id='edit_profile'>Edit profile</button>" +
		                "<div class='clearfix'></div>" +
		              "</div>";
    			$("#prof_user_info").append(info);
		}else {
			Command: toastr["error"]("Something wrong has happened, please try latter", "Error")

			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "400",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}	
		}
		$(document).on('click','#edit_profile', function () {
			$("#prof_user_dropzone").show();
			$("#prof_user_info").empty();
			$("#prof_img_info").empty();
			var info = "<div class='w3_two_grid_left1 cesta'>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>Username: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_username' readonly='' value='" + json[0]['user'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>Email: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_email' readonly='' value='" + json[0]['email'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>Name: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_name' value='" + json[0]['name'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>Last Name: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_lastname'value='" + json[0]['lastname'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>City: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_city' value='" + json[0]['city'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<div class='control-group form-group'>" +
			                "<div class='controls'>" +
			                  "<label class='contact-p1'>Date Birthday: </label>" +
			                  "<label id='error_discid'>" +
			                    "<input class='form-control' type='text' name='' id='profile_date_birthday' value='" + json[0]['date_birthday'] + "'/>" +
			                    "<div></div>" +
			                  "</label>" +
			                "</div>" +
			            "</div>" +
		                "<button id='save_edit_profile'>Save</button>" +
		                "<button id='cancel_edit_profile'>Cancel</button>" +
		                "<div class='clearfix'></div>" +
		              "</div>";
				$("#prof_user_info").append(info);

				var img = "<img src='" + json[0]['avatar'] + "'/>";
				$("#prof_img_info").append(img);
		});
		$(document).on('click','#save_edit_profile', function () {
			$("#profile_username").keyup(function () {
		        if ($(this).val().length != "") {
		            $(".error").fadeOut();
		            return false;
		        }
		    });

		    $("#profile_email").keyup(function () {
		        if ($(this).val().length != "") {
		            $(".error").fadeOut();
		            return false;
		        }
		    });
			var user_reg = /^[a-zA-Z0-9ü][a-zA-Z0-9ü_]{2,15}$/;

		    $(".error").remove();
			if (!user_reg.test($("#profile_name").val())) {
		        $("#profile_name").focus().after("<span class='error'>Username must be 2 to 15 letters</span>");
		        return false;
		    }

			if (!user_reg.test($("#profile_lastname").val())) {
		        $("#profile_lastname").focus().after("<span class='error'>Username must be 2 to 15 letters</span>");
		        return false;
		    }

			if (!user_reg.test($("#profile_city").val())) {
		        $("#profile_city").focus().after("<span class='error'>Username must be 2 to 15 letters</span>");
		        return false;
		    }

		    var username = $("#profile_username").val();
		    var email = $("#profile_email").val();
		    var name = $("#profile_name").val();
		    var lastname = $("#profile_lastname").val();
		    var city = $("#profile_city").val();
		    var birthday = $("#profile_date_birthday").val();
		    
			var data = {"user": username, "name": name, "lastname": lastname, "city": city, "birthday": birthday, "avatar": avatar};
			var profile_data_JSON = JSON.stringify(data);
			$.post(amigable("?module=login&function=update_info_prof"), {profile_data_json: profile_data_JSON}, function (response) {
				if(response == "good"){
					$("#prof_user_dropzone").hide();
					window.location.href = amigable("?module=login&function=load_profile");
					Command: toastr["success"]("Your information was changed correctly!", "Congratulations")

					toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-top-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					}
				}else if (response == "error"){
					Command: toastr["error"]("Something wrong has happened, please try latter", "Error")

					toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-top-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					}	
				}else if (response == "error1"){
					Command: toastr["error"]("That age is imposible", "Error")

					toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": false,
					  "positionClass": "toast-top-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "400",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					}		
				}
			},"json").fail(function(xhr, textStatus, errorThrown){
				if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null ){
                	xhr.responseJSON = JSON.parse(xhr.responseText);
				}
				$(".error1").remove();
				if(xhr.responseJSON.img_profile){
            		$("#dropzone").focus().after("<span  class='error1'>" + xhr.responseJSON.img_profile + "</span>");
				}
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
			});
		});
		$(document).on('click','#cancel_edit_profile', function () {
			window.location.href = amigable("?module=login&function=load_profile");
		});
	});
});
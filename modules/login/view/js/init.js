$(document).ready(function () {

	var user = Tools.readCookie("user");
	if (user) {
	    user = user.split("|");
	    $("#LogUser").empty();
	    $("#LogUser").after("<li><a id='logout' href=''>Log Out</a></li>");

	    if (user[2] === "client") {
	        $("#LogUser").before("<li><a href='#'>Cart</a></li>");
	        $("#LogUser").before("<li><a href='../../login/load_profile/'>Profile</a></li>");
	    } else if (user[2] === "admin") {
	        $("#LogUser").before("<li><a href='../../albums/list_crud/'>Music</a></li>");
	        $("#LogUser").before("<li><a href='../../login/load_profile/'>Profile</a></li>");
	    }
	}

});
var config = {
    apiKey: "AIzaSyA5BfauROZx1KD_sQBiBl_FEaTUrF1iQes",
    authDomain: "music-e6c85.firebaseapp.com",
    databaseURL: "https://music-e6c85.firebaseapp.com/__/auth/handler",
    projectId: "music-e6c85",
    storageBucket: "",
    messagingSenderId: "613764177727"
};
firebase.initializeApp(config);

var provider = new firebase.auth.FacebookAuthProvider();

var providerg = new firebase.auth.GoogleAuthProvider();
providerg.addScope('email');

var providert = new firebase.auth.TwitterAuthProvider();

var authService = firebase.auth();

// autentico con Facebook
$(document).on('click','#loginfacebook', function () {
    authService.signInWithPopup(provider)
    .then(function(result) {
        console.log(result.user);

        var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        var social_JSON = JSON.stringify(data);
        $.post(amigable("?module=login&function=social"), {social_json: social_JSON}, function (response) {
          var json = JSON.parse(response);
          if(json == "email_no"){
            Command: toastr["error"]("Your social account is private and we can't see your email", "Error")
            toastrr()
          }else if(json == "error"){
            Command: toastr["error"]("Somthing wrong was happened searching your information on our database", "Error")
            toastrr()
          }else if(json == "email_rep"){
            Command: toastr["error"]("This email was alrready registrated", "Error")
            toastrr()
          }else if(json == "error1"){
            Command: toastr["error"]("Somthing wrong was happened inserting your information on our database", "Error")
            toastrr()
          }else {
              Tools.createCookie("user", json[0]['user'] + "|" + json[0]['avatar'] + "|" + json[0]['type'], 1);
              window.location.href = amigable("?module=login&function=load_profile");
          }
        });
    })
    .catch(function(error) {
        console.log('Detectado un error:', error);
    });
})

// autentico con Google
$(document).on('click','#botonlogin', function () {
  authService.signInWithPopup(providerg)
    .then(function(result) {
        console.log(result.user);

        var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        var social_JSON = JSON.stringify(data);
        $.post(amigable("?module=login&function=social"), {social_json: social_JSON}, function (response) {
          var json = JSON.parse(response);
          if(json == "email_no"){
            Command: toastr["error"]("Your social account is private and we can't see your email", "Error")
            toastrr()
          }else if(json == "error"){
            Command: toastr["error"]("Somthing wrong was happened searching your information on our database", "Error")
            toastrr()
          }else if(json == "email_rep"){
            Command: toastr["error"]("This email was alrready registrated", "Error")
            toastrr()
          }else if(json == "error1"){
            Command: toastr["error"]("Somthing wrong was happened inserting your information on our database", "Error")
            toastrr()
          }else {
              Tools.createCookie("user", json[0]['user'] + "|" + json[0]['avatar'] + "|" + json[0]['type'], 1);
              window.location.href = amigable("?module=login&function=load_profile");
          }
        });
    })
    .catch(function(error) {
        console.log('Se ha encontrado un error:', error);
    });
})

// autentico con Twitter
$(document).on('click','#logintwitter', function () {
  authService.signInWithPopup(providert)
    .then(function(result) {
        console.log(result.user);

        var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        var social_JSON = JSON.stringify(data);
        $.post(amigable("?module=login&function=social"), {social_json: social_JSON}, function (response) {
          var json = JSON.parse(response);
          if(json == "email_no"){
            Command: toastr["error"]("Your social account is private and we can't see your email", "Error")
            toastrr()
          }else if(json == "error"){
            Command: toastr["error"]("Somthing wrong was happened searching your information on our database", "Error")
            toastrr()
          }else if(json == "email_rep"){
            Command: toastr["error"]("This email was alrready registrated", "Error")
            toastrr()
          }else if(json == "error1"){
            Command: toastr["error"]("Somthing wrong was happened inserting your information on our database", "Error")
            toastrr()

          }else {
                Tools.createCookie("user", json[0]['user'] + "|" + json[0]['avatar'] + "|" + json[0]['type'], 1);
                window.location.href = amigable("?module=login&function=load_profile");
          }
        });
    })
    .catch(function(error) {
          console.log('Se ha encontrado un error:', error);
    });
})

function toastrr(){
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
$(document).ready(function () {
	$("#submitLog").click(function () {
        login();
    });

    $(".btn_red_reg").click(function () {
        window.location.href=amigable("?module=login&function=register");
    });

    $(".btn_recover_pass").click(function () {
        window.location.href=amigable("?module=login&function=load_recover_pass");
    });

    $("#inputUser").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#inputPass").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });
});

function login() {
    var user = $("#inputUser").val();
    var pass = $("#inputPass").val();
    var value = false;

    $(".error").remove();
    if (!user) {
        $("#inputUser").focus().after("<span class='error'>Error empty username</span>");
        value = false;
    }else if (!pass) {
      $("#inputPass").focus().after("<span class='error'>Error password empty</span>");
      value = false;
    }else {
      value = true;
    }
    
    var data = {"user": user, "pass": pass};
    var login_JSON = JSON.stringify(data);
    if (value){
      $('#submitLog').attr('disabled', true);
      $.post(amigable("?module=login&function=access"), {login_json: login_JSON}, function (response) {
          var json = JSON.parse(response);
          if (json.success == true) {
              Tools.createCookie("user", json.data[0]['user'] + "|" + json.data[0]['avatar'] + "|" + json.data[0]['type'], 1);
              window.location.href = amigable("?module=login&function=load_profile");
          }else if (json == "error"){
            $('#submitLog').attr('disabled', false);
              Command: toastr["warning"]("The username or the password are incorrectly.", "Error")
              toastrr();
          }else if (json == "error1"){
            $('#submitLog').attr('disabled', false);
              Command: toastr["warning"]("The account has not been activated check the email.", "Error")
              toastrr();
          }else if (json.success == false) {
            $('#submitLog').attr('disabled', false);
              Command: toastr["warning"]("Something wrong was hapen try latter please.", "Error")
              toastrr();

          }
      });
    }
}
function toastrr() {
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-left",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
$(document).ready(function () {

	$(document).on('click','#modify_pass_btn', function () {
		change_pass();
	});

	$("#modify_pass").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#modify_pass2").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

});

function change_pass() {
    var pass = $("#modify_pass").val();
    var pass2 = $("#modify_pass2").val();
    var value = true;

    var passwd_reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,15}$/;

    $(".error").remove();

    if ($("#modify_pass").val() === "" || $("#modify_pass").val() === "Input a password") {
        $("#modify_pass").focus().after("<span class='error'>Input a password</span>");
        return false;
    }else if (!passwd_reg.test($("#modify_pass").val())) {
        $("#modify_pass").focus().after("<span class='error'>Password must be 8 to 15 characters and be like Password12.</span>");
        return false;
    }else if ($("#modify_pass").val() !== $("#modify_pass2").val()){
        $("#modify_pass").focus().after("<span class='error'>Two password must be the same</span>");
        return false;
    }

    if ($("#modify_pass2").val() === "" || $("#modify_pass2").val() === "Input a password") {
        $("#modify_pass2").focus().after("<span class='error'>Input a password</span>");
        return false;
    }else if (!passwd_reg.test($("#modify_pass2").val())) {
        $("#modify_pass2").focus().after("<span class='error'>Password must be 8 to 15 characters and be like Password12.</span>");
        return false;
    }
    
    if (value){
    	$('#modify_pass_btn').attr('disabled', true);
        $.post(amigable("?module=login&function=change_pass"), {pass: pass}, function (response) {
            var json = JSON.parse(response);
            if(json == "good"){
            	Command: toastr["success"]("Your password has been changed correctly", "Congratulations")
              toastrr()
              setTimeout(function(){ window.location=amigable("?module=login&function=load_login");}, 3000);
            }else if(json == "error"){
            	Command: toastr["error"]("Something wrong has happened, please try latter", "Error")
              toastrr()
            }else if(json == "error1"){
              Command: toastr["error"]("You have already changed the password", "Error")
              toastrr()
            }
        });
    }
}

function toastrr(){
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
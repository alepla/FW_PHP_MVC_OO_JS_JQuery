$(document).ready(function () {
	$("#us_btn").click(function () {
        register();
    });

    $("#us_username").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#us_password").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#us_email").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#us_password2").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });
});

function register() {
    var user = $("#us_username").val();
    var pass = $("#us_password").val();
    var pass2 = $("#us_password2").val();
    var email = $("#us_email").val();
    var res = true;

    var passwd_reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,15}$/;
    var user_reg = /^[a-zA-Z0-9ü][a-zA-Z0-9ü_]{3,9}$/;

    $(".error").remove();
    if ($("#us_username").val() === "" || $("#us_username").val() === "Input a username") {
        $("#us_username").focus().after("<span class='error'>Input a username</span>");
        return false;
    }else if (!user_reg.test($("#us_username").val())) {
        $("#us_username").focus().after("<span class='error'>Username must be 3 to 9 letters</span>");
        return false;
    }

    if ($("#us_password").val() === "" || $("#us_password").val() === "Input a password") {
        $("#us_password").focus().after("<span class='error'>Input a password</span>");
        return false;
    }else if (!passwd_reg.test($("#us_password").val())) {
        $("#us_password").focus().after("<span class='error'>Password must be 8 to 15 characters and be like Password12.</span>");
        return false;
    }

    if ($("#us_password2").val() === "" || $("#us_password2").val() === "Input a password") {
        $("#us_password2").focus().after("<span class='error'>Input a password</span>");
        return false;
    }else if (!passwd_reg.test($("#us_password2").val())) {
        $("#us_password2").focus().after("<span class='error'>Password must be 8 to 15 characters and be like Password12.</span>");
        return false;
    }

    if ($("#us_email").val() === "" || $("#us_email").val() === "Input a email") {
        $("#us_email").focus().after("<span class='error'>Input a email</span>");
        return false;
    }
    
    var data = {"user": user, "pass": pass, "pass2": pass2, "email": email};
    var register_data_JSON = JSON.stringify(data);
    if (res){
      $('#us_btn').attr('disabled', true);
    	$.post(amigable("?module=login&function=register_user"), {register_data_json: register_data_JSON}, function (response) {
            if (response.success) {
                Command: toastr["success"]("Now you are in Pla Music, checkout your email", "Congratulations!")
                toatrr()
                setTimeout(function(){ window.location=amigable("?module=main&function=load_main");}, 3000);
            }
        },"json").fail(function(xhr, textStatus, errorThrown){
            $('#us_btn').attr('disabled', false);
            Command: toastr["error"]("Something wrong has happened..", "Error")
            toatrr()
            $(".error1").remove();

            if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null ){
                xhr.responseJSON = JSON.parse(xhr.responseText);
            }

            if(xhr.responseJSON.error.user){
                $("#error_username").focus().after("<span  class='error1'>" + xhr.responseJSON.error.user + "</span>");
            }
            if(xhr.responseJSON.error.pass){
                $("#error_password1").focus().after("<span  class='error1'>" + xhr.responseJSON.error.pass + "</span>");
            }
            if(xhr.responseJSON.error.email){
                $("#error_email").focus().after("<span  class='error1'>" + xhr.responseJSON.error.email + "</span>");
            }
        });
    }
}

function toatrr(){
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "400",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
$(document).ready(function () {

	$(document).on('click','#recover_btn', function () {
		recover_pass();
	});

	$("#recover_email").keyup(function () {
        if ($(this).val().length != "") {
            $(".error").fadeOut();
            return false;
        }
    });

});

function recover_pass() {
    var email = $("#recover_email").val();
    var res = true;

    $(".error").remove();

    if ($("#recover_email").val() === "" || $("#recover_email").val() === "Input a email") {
        $("#recover_email").focus().after("<span class='error'>Input a email</span>");
        return false;
    }

    if (res){
  		$('#recover_btn').attr('disabled', true);
    	$.post(amigable("?module=login&function=recover_pass"), {email: email}, function (response) {
    		var json = JSON.parse(response);
    		if(json == "good") {
			    Command: toastr["info"]("Check your email to change the password.", "Message")
			    toastrr()
    		}else if(json == "error") {
    			$('#recover_btn').attr('disabled', false);
    			Command: toastr["error"]("Not valid email", "Error")
    			toastrr()
    		}else if (json == "error1") {
    			$('#recover_btn').attr('disabled', false);
    			Command: toastr["error"]("This email doesnt exist", "Error")
    			toastrr()
    		}else if (json == "error2") {
    			$('#recover_btn').attr('disabled', false);
    			Command: toastr["error"]("Sorry, something wrong happens", "Error")
    			toastrr()
    		}
    	});
        
    }
}

function toastrr(){
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "400",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
}
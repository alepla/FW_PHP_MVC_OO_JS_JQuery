$(document).ready(function () {
    $("#logout").click(function () {
        Tools.eraseCookie("user");
        window.location.href = amigable("?module=main&function=load_main");
    });
});
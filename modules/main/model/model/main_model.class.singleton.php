<?php

class main_model {
    private $bll;
    static $_instance;
    
    private function __construct() {
        $this->bll = main_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function page_main($position) {
        return $this->bll->page_main_BLL($position);
    }

    public function autocomplete_main() {
        return $this->bll->autocomplete_main_BLL();
    }

    public function id_main($id) {
        return $this->bll->id_main_BLL($id);
    }
}

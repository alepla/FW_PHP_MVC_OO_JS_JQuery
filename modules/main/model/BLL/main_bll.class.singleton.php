<?php

class main_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = main_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function page_main_BLL($position) {
        return $this->dao->page_main_DAO($this->db, $position);
    }

    public function autocomplete_main_BLL() {
        return $this->dao->autocomplete_main_DAO($this->db);
    }

    public function id_main_BLL($id) {
        return $this->dao->id_main_DAO($this->db, $id);
    }
}


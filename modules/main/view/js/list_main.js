$(document).ready(function () {
    var track_click = 0;
    $.post(amigable("?module=main&function=page"), {'page': track_click}, function (data, status) {
        track_click++;
        var json = JSON.parse(data);
        for(var i in json){
            var row = "<div class='lis'>" +
                        "<div class='lis2'>" +
                            "<a class='click_img_list' id=" + json[i].discid +">" +
                            "<img class='img_lis' src=" + json[i].discpic + "></img></a>" +
                            "<p>" + json[i].discname + "</p>" +
                        "</div>" +
                      "</div>";
            $("#result").append(row);
        }
    });
    $(".load_more").click(function (e) {
        if(track_click <= 3){
            $.post(amigable("?module=main&function=page"), {'page': track_click}, function (data, status) {
                var jso = JSON.parse(data);
                for(var j in jso){
                    var row = "<div class='lis'>" +
                                "<div class='lis2'>" +
                                    "<a class='click_img_list' id=" + jso[j].discid +">" +
                                    "<img class='img_lis' src=" + jso[j].discpic + "></img></a>" +
                                    "<p>" + jso[j].discname + "</p>" +
                                "</div>" +
                              "</div>";
                    $("#result").append(row);
                }  
                track_click++;
            });
            if(track_click >= 3-1){
                $(".load_more").hide();
            }
        }
    });
    $(document).on('click','.click_img_list', function () {
        var id = this.getAttribute('id');
        $.post(amigable("?module=main&function=id"), {'id': id}, function (data, status) {
            var name = JSON.parse(data);
            var keyword = name[0].discname;
            $.post(amigable("?module=main&function=key"), {'key': keyword}, function (data, status) {
                window.location.href = amigable("?module=shop&function=list_shop");
            });
        });
    });

    $("#search_prod").submit(function (e) {
        var keyword = document.getElementById('keyword').value;
        $.post(amigable("?module=main&function=key"), {'key': keyword}, function (data, status) {
            window.location.href = amigable("?module=shop&function=list_shop");
        });
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        $.post(amigable("?module=main&function=key"), {'key': keyword}, function (data, status) {
            window.location.href = amigable("?module=shop&function=list_shop");
        });
    });
    $.post(amigable("?module=main&function=autocomplete"), {'autocomplete': true}, function (data, status) {
        var autocomplete_discs = JSON.parse(data);
        var suggestions = new Array();
        for (var i = 0; i < autocomplete_discs.length; i++) {
            suggestions.push(autocomplete_discs[i].discname);
        }
        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                var keyword = ui.item.label;
                $.post(amigable("?module=main&function=key"), {'key': keyword}, function (data, status) {
                    window.location.href = amigable("?module=shop&function=list_shop");
                });
            }
        });
    });
});
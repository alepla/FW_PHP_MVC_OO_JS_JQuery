<?php

//@session_start();
class controller_main {

    function __construct() {
        $_SESSION['module'] = "main";
    }

    function load_main() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/main/view/', 'list_main.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }
/////////////////////////////////////////SCROLL
    function page() {
        if (isset($_POST["page"])) {
            $page_number = $_POST["page"];
            $item_per_page = 3;
            $position = ($page_number * $item_per_page);
            $arrValue = loadModel(MODEL_MAIN, "main_model", "page_main", $position);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }
    }
///////////////////////////////////////////AUTOCOMPLETE
    function autocomplete() {
        if (isset($_POST["autocomplete"]) && ($_POST["autocomplete"] == true)) {
            $arrValue = loadModel(MODEL_MAIN, "main_model", "autocomplete_main");
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }
    }

    function key() {
        if (isset($_POST["key"])) {
            $gen = $_POST["key"];
            $_SESSION['selected_key'] = $gen;
            echo json_encode($_SESSION['selected_key']);
        }
    }

    function id () {
        if (isset($_POST["id"])) {
            $id = $_POST["id"];
            $arrValue = loadModel(MODEL_MAIN, "main_model", "id_main", $id);
            if ($arrValue) {
                echo json_encode($arrValue);
                exit;
            } else {
                echo json_encode("error");
                exit;
            }
        }
    }

}
var id = "";
$(document).ready(function () {
	if (getCookie("initMap2")) {
        var keyword=getCookie("initMap2");
        var keyword2=getCookie("initMap");
        initMap2(keyword, keyword2);
        setCookie("initMap2", "", "", 1);
    } else {
        initMap();
    }
	$.post(amigable("?module=map&function=list_albums"), function(data) {
		var json = JSON.parse(data);
	    for(var i in json){
	        var row = "<div class='w3_two_grid_left1 listt'>" +
	                    "<div class='col-xs-3 w3_two_grid_left_grid img_alb'><img src=" + json[i].discpic + "></div>" +
	                    "<h2 class='name_prod' id='" + json[i].discname + "'>" + json[i].discname + "</h2>" +
	                    "<h2 class='studies_prod'>" + json[i].discprice + ".00$</h2>" +
	                    "<p class='dessc'>" + json[i].discdesc + "</p>" +
	                    "<div class='clearfix'></div>" +
	                  "</div>";

	        $("#albums_map").append(row);
	    }
	    $(document).on('click','.name_prod', function () {
	    	//id="";
	    	id = this.getAttribute('id');
		});
	});
	$(document).on('click','.cla', function () {
		$( "#asdr" ).empty();
		var mod = this.getAttribute('id');
		$.post(amigable("?module=map&function=idMap"), {'idProduct': mod}, function (data) {
            var json = JSON.parse(data);
            var album = json.album;
            var ro = "<div class='w3_two_grid_left1 mod'>" +
                            "<div class='col-xs-3 w3_two_grid_left_grid img_mod'><img src=" + album.discpic + "></div>" +
                            "<h4 class='name_prod'>" + album.discname + "</h4>" +
                            "<h4 class='studies_prod'>" + album.artist + "</h4>" +
                            "<h3 class='stock'>" + album.discprice + ".00$</h3>" +
                            "<p class='stock'>" + album.discdesc + "</p>" +
                            "<div class='clearfix'></div>" +
                          "</div>";
            $("#asdr").append(ro);
        });
	});

	$(document).on('click','.btn_map_back', function () {
		window.location=amigable("?module=shop&function=list_shop");
	});

	$.getJSON("../../modules/map/resources/ListOfCitysSpain.json", function (data, status) {
        var suggestions = new Array();
        for (var i = 0; i < data.length; i++) {
            suggestions.push(data[i].nm);
        }
        $("#keyword_map").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                var keyword = ui.item.label;
                $.post(amigable("?module=map&function=autocomplete"), {'auto': keyword}, function(data) {
                	var cor = JSON.parse(data);
                	console.log(cor.lat);
                	console.log(cor.long);
                	setCookie("initMap2", cor.lat, 1);
                	setCookie("initMap", cor.long, 1);
                	location.reload(true);
                });
            }
        });
    });
});
var customLabel = {
    album: {
		label: 'A'
    }
  };

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 40.329428, lng: -3.656641},
      zoom: 9
    });
    var infoWindow = new google.maps.InfoWindow({map: map});
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			infoWindow.setPosition(pos);
			infoWindow.setContent('Location found.');
			map.setCenter(pos);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
    } else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
    }

	// Change this depending on the name of your PHP or XML file
	downloadUrl(amigable("?module=map&function=select_lat_long"), function(data) {
		var xml = data.responseXML;
		var markers = xml.documentElement.getElementsByTagName('marker');
		Array.prototype.forEach.call(markers, function(markerElem) {
			var link = "See more information...";
			var name = markerElem.getAttribute('name');
			var address = markerElem.getAttribute('discdesc');
			var type = markerElem.getAttribute('type');
			var point = new google.maps.LatLng(
				parseFloat(markerElem.getAttribute('lat')),
				parseFloat(markerElem.getAttribute('lng')));

			var infowincontent = document.createElement('div');
			var strong = document.createElement('strong');
			strong.textContent = name
			infowincontent.appendChild(strong);
			infowincontent.appendChild(document.createElement('br'));

			var text = document.createElement('text');
			text.textContent = type
			infowincontent.appendChild(text);

			infowincontent.appendChild(document.createElement('br'));
			var lin = document.createElement('a');
			lin.setAttribute("id", name);
			lin.setAttribute("class", "cla");
			lin.setAttribute("data-target", "#exampleModal");
			lin.setAttribute("data-toggle", "modal");
			lin.textContent = link
			infowincontent.appendChild(lin);

			var icon = customLabel[type] || {};
			var marker = new google.maps.Marker({
				map: map,
				position: point,
				label: icon.label
			});
			marker.addListener('click', function() {
				infoWindow.setContent(infowincontent);
				infoWindow.open(map, marker);
				id="";
			});
			$(document).on('click','.name_prod', function () {
				if (name == id){
					infoWindow.setContent(infowincontent);
					infoWindow.open(map, marker);
					id="";
				}
			});
		});
	});
}


function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
	    new ActiveXObject('Microsoft.XMLHTTP') :
	    new XMLHttpRequest;

	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			request.onreadystatechange = doNothing;
			callback(request, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}

function doNothing() {}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
          'Error: The Geolocation service failed.' :
          'Error: Your browser doesn\'t support geolocation.');
}

function initMap2(keyword, keyword2) {
	var a = parseFloat(keyword);
	var b = parseFloat(keyword2);
   	var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(a, b),
          zoom: 9
        });
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
	downloadUrl(amigable("?module=map&function=select_lat_long"), function(data) {
		var xml = data.responseXML;
		var markers = xml.documentElement.getElementsByTagName('marker');
		Array.prototype.forEach.call(markers, function(markerElem) {
			var link = "See more information...";
			var name = markerElem.getAttribute('name');
			var address = markerElem.getAttribute('discdesc');
			var type = markerElem.getAttribute('type');
			var point = new google.maps.LatLng(
				parseFloat(markerElem.getAttribute('lat')),
				parseFloat(markerElem.getAttribute('lng')));

			var infowincontent = document.createElement('div');
			var strong = document.createElement('strong');
			strong.textContent = name
			infowincontent.appendChild(strong);
			infowincontent.appendChild(document.createElement('br'));

			var text = document.createElement('text');
			text.textContent = type
			infowincontent.appendChild(text);

			infowincontent.appendChild(document.createElement('br'));
			var lin = document.createElement('a');
			lin.setAttribute("id", name);
			lin.setAttribute("class", "cla");
			lin.setAttribute("data-target", "#exampleModal");
			lin.setAttribute("data-toggle", "modal");
			lin.textContent = link
			infowincontent.appendChild(lin);

			var icon = customLabel[type] || {};
			var marker = new google.maps.Marker({
				map: map,
				position: point,
				label: icon.label
			});
			marker.addListener('click', function() {
				infoWindow.setContent(infowincontent);
				infoWindow.open(map, marker);
			});
		});
	});
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return 0;
}
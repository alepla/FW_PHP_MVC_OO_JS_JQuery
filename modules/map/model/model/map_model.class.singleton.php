<?php

class map_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = map_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_map() {
        return $this->bll->list_map_BLL();
    }

   public function details_map($id) {
        return $this->bll->details_map_BLL($id);
    }

}

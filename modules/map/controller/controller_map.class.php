<?php

class controller_map {
    function __construct() {
        $_SESSION['module'] = "map";
    }

    function load_map() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/map/view/', 'list_map.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function select_lat_long () {
    	$dom = new DOMDocument("1.0");
		$node = $dom->createElement("markers");
		$parnode = $dom->appendChild($node);
        $arrValue = loadModel(MODEL_MAP, "map_model", "list_map");

        header("Content-type: text/xml");

        foreach ($arrValue as $value) {
            $node = $dom->createElement("marker");
            $newnode = $parnode->appendChild($node);
            $newnode->setAttribute("name", $value['discname']);
            $newnode->setAttribute("desc", $value['discdesc']);
            $newnode->setAttribute("lat", $value['lat']);
            $newnode->setAttribute("lng", $value['lng']);
            $newnode->setAttribute("type", $value['type']);
        }
        echo $dom->saveXML();
    }

    function list_albums() {
        $arrValue = loadModel(MODEL_MAP, "map_model", "list_map");
        if ($arrValue){
            echo json_encode($arrValue);
            exit();
        }else{
            echo json_encode("error");
            exit();
        }
    }

    function idMap() {
        if ($_POST["idProduct"]) {
            $id = $_POST["idProduct"];
            $arrValue = loadModel(MODEL_MAP, "map_model", "details_map", $id);
            if ($arrValue) {
                $jsondata["album"] = $arrValue[0];
                echo json_encode($jsondata); 
                exit();
            }else {
                echo json_encode("error");
                exit();
            }
        } 
    }

    function autocomplete() {

        $direccion = $_POST["auto"];
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        // Obtener los resultados JSON de la peticion.
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($direccion).'&key=AIzaSyDAhlrtCRkZQAmCe13cg4fHOWDE3eBAliY', false, stream_context_create($arrContextOptions));
        // Convertir el JSON en array.
        $geo = json_decode($geo, true);

        // Si todo esta bien
        if ($geo['status'] = 'OK') {
            // Obtener los valores
            $latitud = $geo['results'][0]['geometry']['location']['lat'];
            $longitud = $geo['results'][0]['geometry']['location']['lng'];
        }
        $cor = array('lat' => $latitud,
                     'long' => $longitud
                );

        echo json_encode($cor);
    }
}
$(document).ready(function () {
    $('#contact_btn').click(function(){
        validate_contact();
    });

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    var email_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    var string_msg = /^[0-9A-Za-z]{10,250}$/;

    $("#name_contact").keyup(function () {
      if ($(this).val() !== "" && string_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#email_contact").keyup(function () {
      if ($(this).val() !== "" && email_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#message_contact").keyup(function () {
      if ($(this).val() !== "" && string_msg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });
});

function validate_contact(){
	var result = true;

  var name_contact = document.getElementById('name_contact').value;
	var email_contact = document.getElementById('email_contact').value;
	var message_contact = document.getElementById('message_contact').value;
	var s = document.getElementById('subject_contact');
  var subject_contact = s.options[s.selectedIndex].text;

  var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
	var email_reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	var string_msg = /^(.){10,250}$/;

	$(".error").remove();

  if ($("#name_contact").val() === "" || $("#name_contact").val() === "Input a full name") {
      $("#name_contact").focus().after("<span class='error'>Input a full name</span>");
      return false;
  }else if (!string_reg.test($("#name_contact").val())) {
      $("#name_contact").focus().after("<span class='error'>Input a full name</span>");
      return false;
  }
  if ($("#email_contact").val() === "" || $("#email_contact").val() === "Input a email") {
      $("#email_contact").focus().after("<span class='error'>Input a email</span>");
      return false;
  }else if (!email_reg.test($("#email_contact").val())) {
      $("#email_contact").focus().after("<span class='error'>Input a email</span>");
      return false;
  }

  if ($("#subject_contact").val() === "" || $("#subject_contact").val() === "Choose one subject" || $("#subject_contact").val() === null) {
      $("#subject_contact").focus().after("<span class='error'>Select one subject</span>");
      return false;
  }

  if ($("#message_contact").val() === "" || $("#message_contact").val() === "Input a message") {
      $("#message_contact").focus().after("<span class='error'>Input a message</span>");
      return false;
  }else if (!string_msg.test($("#message_contact").val())) {
      $("#message_contact").focus().after("<span class='error'>Message cannot be empty</span>");
      return false;
  }
  
  if (result){
    paint();
    var data = {"name_contact": name_contact, "email_contact": email_contact, "subject_contact": subject_contact, "message_contact": message_contact};
    var data_contact_JSON = JSON.stringify(data);
    $.post(amigable("?module=contact&function=send_contact"), {data_contact_json:data_contact_JSON}, function (data){
    	if (data == "okok"){
            var info = "<div class='alert alert-success' role='alert'>" +
                        "<p>Youre message has been send succesfully!</p>" +
                       "</div>";
            $(".alert_contact").append(info);
            setTimeout(function(){ window.location=amigable("?module=main&function=load_main");}, 3000);
      }else{
            $('#contact_btn').attr('disabled', false);
            var info = "<div class='alert alert-danger' role='alert'>" +
                        "<p>Sorry, there have been some error!</p>" +
                       "</div>";
            $(".alert_contact").append(info);
      }
    });
  }
}

function paint() {
  $('#contact_btn').attr('disabled', true);
}
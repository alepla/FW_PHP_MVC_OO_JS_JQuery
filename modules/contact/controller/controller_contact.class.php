<?php

class controller_contact {
  function __construct() {
      $_SESSION['module'] = "contact";
  }

  function list_contact() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");
      loadView('modules/contact/view/', 'list_contact.html');
      require_once(VIEW_PATH_INC . "footer.html");
  }

  function send_contact(){
  	$contactJSON = json_decode($_POST["data_contact_json"], true);
    //Mail for the user
  	$arrArgument = array(
      'type' => 'contact',
      'token' => '',
      'name' => $contactJSON['name_contact'],
  		'email' => $contactJSON['email_contact'],
  		'subject' => $contactJSON['subject_contact'],
  		'message' => $contactJSON['message_contact']
  	);
    if ($arrArgument){
      echo "ok";
      send_mail($arrArgument);
    } else {
      echo "bad";
    }

    //Mail for the admin
    $arrArgument = array(
      'type' => 'admin',
      'token' => '',
      'name' => $contactJSON['name_contact'],
      'email' => $contactJSON['email_contact'],
      'subject' => $contactJSON['subject_contact'],
      'message' => $contactJSON['message_contact']
    );
    if ($arrArgument){
      echo "ok";
      send_mail($arrArgument);
    } else {
      echo "bad";
    }
  }
}
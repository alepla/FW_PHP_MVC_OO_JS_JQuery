<?php

class albums_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = albums_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_album($arrArgument) {
        return $this->bll->create_albums_BLL($arrArgument);
    }

    public function obtain_artists($url){
        return $this->bll->obtain_artists_BLL($url);
    }

    public function obtain_genres(){
        return $this->bll->obtain_genres_BLL();
    }

    public Function obtain_subgenres($arrArgument){
        return $this->bll->obtain_subgenres_BLL($arrArgument);
    }

    public Function sele_id($id){
        return $this->bll->sele_id_BLL($id);
    }

    public Function list_all_albums(){
        return $this->bll->list_all_albums_BLL();
    }

    public Function read_album($id){
        return $this->bll->read_album_BLL($id);
    }

    public Function delete_album($id){
        return $this->bll->delete_album_BLL($id);
    }

    public Function update_album($arrArgument){
        return $this->bll->update_album_BLL($arrArgument);
    }

}

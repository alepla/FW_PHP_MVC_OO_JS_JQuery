<?php

class albums_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function sele_id_DAO($db, $id) {
        $sql = "SELECT discid FROM albums WHERE discid=".$id;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function create_album_DAO($db, $arrArgument) {
        $discname = $arrArgument['discname'];
        $discid = $arrArgument['discid'];
        $discprice = $arrArgument['discprice'];
        $date_entry = $arrArgument['date_entry'];
        $date_expiration = $arrArgument['date_expiration'];
        $format = $arrArgument['format'];
        $valoration = $arrArgument['valoration'];
        $artist = $arrArgument['artist'];
        $genre = $arrArgument['genre'];
        $subgenre = $arrArgument['subgenre'];
        $discdesc = $arrArgument['discdesc'];
        $discpic = $arrArgument['prodpic'];

        $format = '';
        foreach ($arrArgument[format] as $type) {
            $format = $format. $type. " ";
        }
        
        $sql = "INSERT INTO albums (discname, discid, discprice, date_entry,"
                . " date_expiration, format, valoration, discdesc, artist, genre, subgenre, discpic, n_views) VALUES ('$discname', '$discid',"
                . " '$discprice', '$date_entry', '$date_expiration', '$format', '$valoration', '$discdesc', '$artist', '$genre', '$subgenre',"
                . " '$discpic', '0')";

        return $db->ejecutar($sql);
    }

    public function obtain_artists_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }

    public function obtain_genres_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file(RESOURCES . 'genresandsubgenres.xml');
          $result = $provincias->xpath("/lista/genre/nombre | /lista/genre/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
          return $json;
    }

    public function obtain_subgenres_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file(RESOURCES . 'genresandsubgenres.xml');
          $result = $xml->xpath("/lista/genre[@id='$filter']/subgenres");

          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'subgenre' => (string) $result[0]->subgenre[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }

    public function list_all_albums_DAO($db) {
        $sql = "SELECT discid, discname FROM albums";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function read_album_DAO($db, $id) {
        $sql = "SELECT * FROM albums WHERE discid=".$id;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function delete_album_DAO($db, $id) {
        $sql = "DELETE FROM albums WHERE discid=".$id;
        return $db->ejecutar($sql);
        
    }

    public function update_album_DAO($db, $arrArgument) {
        $discname = $arrArgument['discname'];
        $discid = $arrArgument['discid'];
        $discprice = $arrArgument['discprice'];
        $date_entry = $arrArgument['date_entry'];
        $date_expiration = $arrArgument['date_expiration'];
        $format = $arrArgument['format'];
        $valoration = $arrArgument['valoration'];
        $artist = $arrArgument['artist'];
        $genre = $arrArgument['genre'];
        $subgenre = $arrArgument['subgenre'];
        $discdesc = $arrArgument['discdesc'];
        $discpic = $arrArgument['prodpic'];

        $format = '';
        foreach ($arrArgument[format] as $type) {
            $format = $format. $type. " ";
        }
        
        $sql = "UPDATE albums SET discname='$discname', discprice='$discprice', date_entry='$date_entry', date_expiration='$date_expiration', "
               ."format='$format', valoration='$valoration', discdesc='$discdesc', artist='$artist', genre='$genre', subgenre='$subgenre', "
               ."discpic='$discpic' WHERE discid='$discid'";

        return $db->ejecutar($sql);
    }
}

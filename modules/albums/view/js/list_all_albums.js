$(document).ready(function () {

    $.post(amigable("?module=albums&function=list_albums"), function (data) {
        var json = JSON.parse(data);
        print_album(json);
    });

    /*------------------CREATE-------------------------*/

    $(document).on('click','#create', function () {
        window.location = amigable("?module=albums&function=create_albums");
    });

    /*------------------READ-------------------------*/

    $(document).on('click','.read', function () {
        $( "#modal_crud" ).empty();
        var id = this.getAttribute('id');
        $.post(amigable("?module=albums&function=read_modal"), {'read': id}, function (data, status) {
            var json = JSON.parse(data);
            var ro = "<div class='w3_two_grid_left1 mod'>" +
                            "<div class='col-xs-3 w3_two_grid_left_grid img_mod'><img src=" + json[0].discpic + "></div>" +
                            "<h4 class='name_prod'>" + json[0].discname + "</h4>" +
                            "<h4 class='studies_prod'>" + json[0].artist + "</h4>" +
                            "<h3 class='stock'>" + json[0].discprice + ".00$</h3>" +
                            "<p class='stock'>" + json[0].discdesc + "</p>" +
                            "<div class='clearfix'></div>" +
                          "</div>";
            $("#modal_crud").append(ro);
        });
    });

    /*------------------UPDATE-------------------------*/

    $(document).on('click','.update', function () {
        var id = this.getAttribute('id');
        if ( window.localStorage){
            localStorage.id = id;
        }
        window.location = amigable("?module=albums&function=update_albums");
    });

    /*------------------DELETE-------------------------*/

    $(document).on('click','.delete', function () {
        $( "#modal_delete" ).empty();
        var id = this.getAttribute('id');
        $.post(amigable("?module=albums&function=read_modal"), {'read': id}, function (data, status) {
            var jso = JSON.parse(data);
            var ro = "<h2>ID: " + id + " </h2>" +
                     "<h2>Name: " + jso[0].discname + " </h2>" +
                     "<button id='yes_d'>Yes</button>" +
                     "<button id='no_d' data-dismiss='modal'>No</button>";
            $("#modal_delete").append(ro);
        });

        $(document).on('click','#yes_d', function () {
            $.post(amigable("?module=albums&function=delete_album"), {'delete': id}, function (data) {
                var json = JSON.parse(data);
                if(json == "good"){
                    Command: toastr["success"]("The album was deleted.", "Good")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-left",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "400",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    setTimeout(function(){ window.location=amigable("?module=albums&function=list_crud");}, 1500);
                }else {
                    Command: toastr["error"]("Something wrong was happened try latter please.", "Error")
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-left",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "400",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }    
                }
            });
        });
    });
});

function print_album(data) {
    for(var i in data){
        var info = "<div id='borde_album'>" +
                   "<h4>Disc ID: " + data[i].discid + "</h4>" +
                   "<h4>Disc Name: " + data[i].discname + "</h4>" +
                   "<button data-target='#CrudModal' data-toggle='modal' class='read' id='" + data[i].discid + "'>Read</button>" +
                   "<button class='update' id='" + data[i].discid + "'>Update</button>" +
                   "<button data-target='#DeleteModal' data-toggle='modal' class='delete' id='" + data[i].discid + "'>Delete</button>"+
                   "</div>";
        $("#list_crud_albums").append(info);
    }
}

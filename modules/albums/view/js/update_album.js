Dropzone.autoDiscover = false;
$(document).ready(function () {

  var avatar = "";

  $(document).on("focus", "#date_entry", function(){
    $( "#date_entry" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true, changeYear: true,
        minDate: 0, maxDate: "+1M"
    });
  });

  $(document).on("focus", "#date_expiration", function(){
    $( "#date_expiration" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true, changeYear: true,
        minDate: 30, maxDate: "+36M"
    });
  });

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var disc_id = /^[0-9]{5}$/;
    var disc_price = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
    var string_description = /^[0-9A-Za-z]{2,90}$/;

    $("#discid").keyup(function () {
      if ($(this).val() !== "" && disc_id.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discname").keyup(function () {
      if ($(this).val() !== "" && string_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discprice").keyup(function () {
      if ($(this).val() !== "" && disc_price.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discdesc").keyup(function () {
      if ($(this).val() !== "" && string_description.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

  $("#dropzone").dropzone({
      url: amigable("?module=albums&function=upload"),
      params:{'upload':true},
      addRemoveLinks: true,
      maxFileSize: 1000,
      dictResponseError: "An error has occurred on the server",
      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
      init: function () {
          this.on("success", function (file, response) {
              $("#progress").show();
              $("#bar").width('100%');
              $("#percent").html('100%');
              $('.msg').text('').removeClass('msg_error');
              $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
          });
      },
      complete: function (file) {
          /*if(file.status == "success"){
            alert("El archivo se ha subido correctamente: " + file.name);
          }*/
      },
      error: function (file) {
          //alert("Error subiendo el archivo " + file.name);
      },
      removedfile: function (file, serverFileName) {
          var name = file.name;
          $.ajax({
              type: "POST",
              url: amigable("?module=albums&function=delete_pic"),
              data: {"filename":name,"delete":true},
              success: function (data) {
                  $("#progress").hide();
                  $('.msg').text('').removeClass('msg_ok');
                  $('.msg').text('').removeClass('msg_error');
                  $("#e_avatar").html("");

                  var json = JSON.parse(data);
                  if (json.res === true) {
                      var element;
                      if ((element = file.previewElement) !== null) {
                          element.parentNode.removeChild(file.previewElement);
                      } else {
                          return false;
                      }
                  } else {
                      var element2;
                      if ((element2 = file.previewElement) !== null) {
                          element2.parentNode.removeChild(file.previewElement);
                      } else {
                          return false;
                      }
                  }

              }
          });
      }
  });

    load_artist_v1();
    
    $("#genre").empty();
    $("#genre").append('<option value="" selected="selected">' + localStorage.genre + '</option>');
    $("#genre").prop('disabled', true);
    $("#subgenre").empty();
    $("#subgenre").append('<option value="" selected="selected">' + localStorage.subgenre + '</option>');
    $("#subgenre").prop('disabled', true);

    $(document).on("change", "#artist", function(){
        var artist = $(this).val();
        var genre = $("#genre");
        var subgenre = $("#subgenre");
             genre.prop('disabled', false);
             subgenre.prop('disabled', false);
             load_genre_v1();
    });

    $(document).on("change", "#genre", function(){
        var gen = $(this).val();
        if(gen > 0){
            load_subgenre_v1(gen);
        }else{
            $("#subgenre").prop('disabled', false);
        }
    });

  $("#update_album").empty();

  var id = localStorage.id;

  $.post(amigable("?module=albums&function=read_modal"), {'read': id}, function (data, status) {
    var json = JSON.parse(data);
    avatar = json[0].discpic;
    if ( window.localStorage){
      localStorage.artist = json[0].artist;
      localStorage.genre = json[0].genre;
      localStorage.subgenre = json[0].subgenre;
    }
    var arr = [];
    var espacio = " ";
    arr = json[0].format;
    var removed = arr.split(espacio);

    var info = "<div class='control-group form-group'>" +
                "<div class='controls'>" +
                  "<label class='contact-p1'>ID: </label>" +
                  "<label id='error_discid'>" +
                    "<input class='form-control' type='text' name='discid' id='discid' required='required' value='" + json[0].discid + "' disabled/>" +
                    "<div></div>" +
                  "</label>" +
                "</div>" +
              "</div>" +
              "<div class='control-group form-group'>" +
                "<div class='controls'>" +
                  "<label class='contact-p1'>Album name: </label>" +
                  "<label id='error_discname'>" +
                    "<input class='form-control' type='text' name='discname' id='discname' required='required' value='" + json[0].discname + "'/>" +
                    "<div></div>" +
                  "</label>" +
                "</div>" +
              "</div>" +
              "<div class='control-group form-group'>" +
                "<div class='controls'>" +
                  "<label class='contact-p1'>Price: </label>" +
                  "<label id='error_discprice'>" +
                    "<input class='form-control' type='text' name='discprice' id='discprice' required='required' value='" + json[0].discprice + "'/>" +
                    "<div></div>" +
                  "</label>" +
                "</div>" +
              "</div>" +
              "<div class='control-group form-group'>" +
                "<div class='controls'>" +
                  "<label class='contact-p1'>Entry date: </label>" +
                  "<label id='error_date_entry'>" +
                    "<input class='form-control' type='text' name='date_entry' id='date_entry' readonly='readonly' value='" + json[0].date_entry + "'/>" +
                    "<div></div>" +
                  "</label>" +
                "</div>" +
              "</div>" +
              "<div class='control-group form-group'>" +
                "<div class='controls'>" +
                    "<label class='contact-p1'>Expiration date: </label>" +
                    "<label id='error_date_expiration'>" +
                    "<input class='form-control' type='text' name='date_expiration' id='date_expiration' readonly='readonly' value='" + json[0].date_expiration + "'/>" +
                    "<div></div>" +
                  "</label>" +
                "</div>" +
              "</div>" +
              "<label class='contact-p1'><strong>Type:</strong></label>" +
              "<div>" +
                "<label class='contact-p1'>Artist: </label>" +
                "<label id='error_artist'>" +
                  "<select class='form-control sel' name='artist' id='artist'>" +
                    "<option selected>" + json[0].artist + "</option>" +
                  "</select>" +
                  "<div></div>" +
                "</label>" +
              "</div>" +
              "<div>" +
                "<labeL class='contact-p1'>Genre: </label>" +
                "<label id='error_genre'>" +
                  "<select class='form-control sel' name='genre' id='genre'>" +
                    "<option selected>" + json[0].genre + "</option>" +
                  "</select>" +
                  "<div ></div>" +
                "</label>" +
              "</div>" +
              "<div>" +
                "<label class='contact-p1'>Subgenre: </label>" +
                "<label id='error_subgenre'>" +
                  "<select class='form-control sel' name='subgenre' id='subgenre'>" +
                    "<option selected>" + json[0].subgenre + "</option>" +
                  "</select>" +
                  "<div></div>" +
                "</label>" +
              "</div>";
      $("#update_album").append(info);
      if(removed[0] == "CD" || removed[1] == "CD" || removed[2] == "CD" || removed[3] == "CD"){
        var forma = "CD <input type='checkbox' name='format[]' class='formatCheckbox' value='CD' checked>";
        $("#forma").append(forma);
      }else {
        var forma = "CD <input type='checkbox' name='format[]' class='formatCheckbox' value='CD'>";
        $("#forma").append(forma);
      }
      if(removed[0] == "Vinyl" || removed[1] == "Vinyl" || removed[2] == "Vinyl" || removed[3] == "Vinyl"){
        var forma = "Vinyl <input type='checkbox' name='format[]' class='formatCheckbox' value='Vinyl' checked>";
        $("#forma").append(forma);
      }else {
        var forma = "Vinyl <input type='checkbox' name='format[]' class='formatCheckbox' value='Vinyl'>";
        $("#forma").append(forma);
      }
      if(removed[0] == "Mp3" || removed[1] == "Mp3" || removed[2] == "Mp3" || removed[3] == "Mp3"){
        var forma = "Mp3 <input type='checkbox' name='format[]' class='formatCheckbox' value='Mp3' checked>";
        $("#forma").append(forma);
      }else {
        var forma = "Mp3 <input type='checkbox' name='format[]' class='formatCheckbox' value='Mp3'>";
        $("#forma").append(forma);
      }
      if(removed[0] == "Cassete" || removed[1] == "Cassete" || removed[2] == "Cassete" || removed[3] == "Cassete"){
        var forma = "Cassete <input type='checkbox' name='format[]' class='formatCheckbox' value='Cassete' checked>";
        $("#forma").append(forma);
      }else {
        var forma = "Cassete <input type='checkbox' name='format[]' class='formatCheckbox' value='Cassete'>";
        $("#forma").append(forma);
      }
      if(json[0].valoration == "Bad"){
        var val = "<div class='control-group form-group'>" +
                    "<div class='controls'>" +
                      "<label class='contact-p1'>Valoration:</label>" +
                      "<label class='asdf'>" +
                            "Bad <input name='valoration' id='valoration' type='radio' class='valoration' value='Bad' checked>" +
                            "Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Good'>" +
                            "Very Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Very good'>" +
                      "</label>" +
                    "</div>" +
                  "</div>";
        $("#update_album_val").append(val);
      }else if(json[0].valoration == "Good"){
        var val = "<div class='control-group form-group'>" +
                    "<div class='controls'>" +
                      "<label class='contact-p1'>Valoration:</label>" +
                      "<label class='asdf'>" +
                            "Bad <input name='valoration' id='valoration' type='radio' class='valoration' value='Bad'>" +
                            "Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Good' checked>" +
                            "Very Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Very good'>" +
                      "</label>" +
                    "</div>" +
                  "</div>";
        $("#update_album_val").append(val);
      }else if(json[0].valoration == "Very good"){
        var val = "<div class='control-group form-group'>" +
                    "<div class='controls'>" +
                      "<label class='contact-p1'>Valoration:</label>" +
                      "<label class='asdf'>" +
                            "Bad <input name='valoration' id='valoration' type='radio' class='valoration' value='Bad'>" +
                            "Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Good'>" +
                            "Very Good <input name='valoration' id='valoration' type='radio' class='valoration' value='Very good' checked>" +
                      "</label>" +
                    "</div>" +
                  "</div>";
        $("#update_album_val").append(val);
      }
      var des = "<div class='control-group form-group'>" +
                  "<div class='controls'>" +
                    "<label class='contact-p1'>Description: </label>" +
                    "<label id='error_discdesc'>" +
                      "<textarea class='form-control' rows='4' cols='40' name='discdesc' id='discdesc'>" + json[0].discdesc + "</textarea>" +
                      "<div></div>" +
                    "</label>" +
                  "</div>" +
                "</div>";
      $("#update_album_des").append(des);
    });

    $(document).on('click','#save_update', function () {

        var result = true;

        var discname = document.getElementById('discname').value;
        var discid = document.getElementById('discid').value;
        var discprice = document.getElementById('discprice').value;
        var date_entry = document.getElementById('date_entry').value;
        var date_expiration = document.getElementById('date_expiration').value;
        var format = [];
        var inputElements = document.getElementsByClassName('formatCheckbox');
        var j=0;
        for (var i=0; i< inputElements.length; i++){
            if (inputElements[i].checked){
              format[j] = inputElements[i].value;
              j++;
            }
        }
        var valoration = $('input[name="valoration"]:checked').val();

        var c = document.getElementById('artist');
        var artist = c.options[c.selectedIndex].text;
        var subgenre = document.getElementById('subgenre').value;
        var discdesc = document.getElementById('discdesc').value;

        var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
        var val_dates = /\d{2}.\d{2}.\d{4}$/;
        var disc_id = /^[0-9]{5}$/;
        var prod_price = /^[0-9]{1,20}$/;
        var string_description = /^(.){1,250}$/;

        $(".error").remove();
        if ($("#discid").val() === "" || $("#discid").val() === "Input disc id") {
            $("#discid").focus().after("<span class='error'>Input disc id</span>");
            return false;
        }else if (!disc_id.test($("#discid").val())) {
            $("#discid").focus().after("<span class='error'>ID must be 5 numbers</span>");
            return false;
        }

        if ($("#discname").val() === "" || $("#discname").val() === "Input disc name"){
          $("#discname").focus().after("<span class='error'>Input disc name</span>");
          return false;
        }else if(!string_reg.test($("#discname").val())){
          $("#discname").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
          return false;
        }

        if ($("#discprice").val() === "" || $("#discprice").val() === "Input disc price") {
            $("#discprice").focus().after("<span class='error'>Input disc price</span>");
            return false;
        }else if (!prod_price.test($("#discprice").val())) {
            $("#discprice").focus().after("<span class='error'>Price must be numbers</span>");
            return false;
        }

        if ($("#date_entry").val() === "" || $("#date_entry").val() === "Input disc entry date") {
            $("#date_entry").focus().after("<span class='error'>Input disc entry date</span>");
            return false;
        }else if (!val_dates.test($("#date_entry").val())) {
            $("#date_entry").focus().after("<span class='error'>Input disc entry date</span>");
            return false;
        }

        if ($("#date_expiration").val() === "" || $("#date_expiration").val() === "Input dsic expiration date") {
            $("#date_expiration").focus().after("<span class='error'>Input dsic expiration date</span>");
            return false;
        }else if (!val_dates.test($("#date_expiration").val())) {
            $("#date_expiration").focus().after("<span class='error'>Input disc expiration date</span>");
            return false;
        }

        if ($("#discdesc").val() === "" || $("#discdesc").val() === "Input disc description") {
            $("#discdesc").focus().after("<span class='error'>Input disc description</span>");
            return false;
        }else if (!string_description.test($("#discdesc").val())) {
            $("#discdesc").focus().after("<span class='error'>Description cannot be empty</span>");
            return false;
        }

        var p = document.getElementById('genre');
        var genre = p.options[p.selectedIndex].text;

        if ($("#subgenre").val() === "" || $("#subgenre").val() === "Select subgenre") {
            $("#subgenre").focus().after("<span class='error'>Select one subgenre</span>");
            return false;
        }

        if (result){
          var data = {"discname": discname, "discid": discid, "discprice": discprice, "date_entry": date_entry, "date_expiration": date_expiration, "format": format, "valoration": valoration, "artist": artist, "genre": genre, "subgenre": subgenre, "discdesc": discdesc, "avatar": avatar};
          var data_albums_JSON = JSON.stringify(data);
          localStorage.removeItem('id');
          localStorage.removeItem('artist');
          localStorage.removeItem('genre');
          localStorage.removeItem('subgenre');
          $.post(amigable("?module=albums&function=update_alta_albums"), {update_albums_json:data_albums_JSON}, function (response){
            if(response.success){
                window.location.href = amigable("?module=albums&function=list_crud");
            }
          },"json").fail(function(xhr, textStatus, errorThrown){
              if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null ){
                  xhr.responseJSON = JSON.parse(xhr.responseText);
              }

              $(".error1").remove();

              if(xhr.responseJSON.error.discname){
                  $("#error_discname").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discname + "</span>");
              }

              if(xhr.responseJSON.error.discid){
                  $("#error_discid").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discid + "</span>");
              }

              if(xhr.responseJSON.error.discprice){
                  $("#error_discprice").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discprice + "</span>");
              }

              if(xhr.responseJSON.error.date_entry){
                  $("#error_date_entry").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_entry + "</span>");
              }

              if(xhr.responseJSON.error.date_expiration){
                  $("#error_date_expiration").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_expiration + "</span>");
              }

              if(xhr.responseJSON.error.format){
                  $("#error_format").focus().after("<span  class='error1'>" + xhr.responseJSON.error.format + "</span>");
              }

              if(xhr.responseJSON.error.artist){
                  $("#error_artist").focus().after("<span  class='error1'>" + xhr.responseJSON.error.artist + "</span>");
              }

              if(xhr.responseJSON.error.genre){
                  $("#error_genre").focus().after("<span  class='error1'>" + xhr.responseJSON.error.genre + "</span>");
              }

              if(xhr.responseJSON.error.subgenre){
                  $("#error_subgenre").focus().after("<span  class='error1'>" + xhr.responseJSON.error.subgenre + "</span>");
              }

              if(xhr.responseJSON.error.discdesc){
                  $("#error_discdesc").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discdesc + "</span>");
              }

              if(xhr.responseJSON.error_prodpic){
                  $("#prodpic").focus().after("<span  class='error1'>" + xhr.responseJSON.error.prodpic + "</span>");
                  $("#progress").hide();
                  $('.msg').text('').removeClass('msg_ok');
                  $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
              }
          });
        }
    });

    $(document).on('click','#cancel_update', function () {
      localStorage.removeItem('id');
      localStorage.removeItem('artist');
      localStorage.removeItem('genre');
      localStorage.removeItem('subgenre');
      window.location.href = amigable("?module=albums&function=list_crud");
    });
});

function load_artist_v2(cad, post_data) {
    $.post(cad, post_data, function(data) {
        var obj = JSON.parse(data);
        $("#artist").empty();
        $("#artist").append('<option value="" selected="selected">' + localStorage.artist + '</option>');
        $.each(obj, function (i, valor) {
            for (var j = 0; j < valor.artist.length; j++) {
                $("#artist").append("<option value='" + valor.artist[j].playcount + "'>" + valor.artist[j].name + "</option>");
            }
        });
    });
}

function load_artist_v1() {
    $.post(amigable("?module=albums&function=load_artists"), {'load_artist': true}, function( response ) {
        if(response === 'error'){
            load_artist_v2(amigable("?module=resources&function=ListOfArtistsNamesByName.json"));
        }else{
            load_artist_v2(amigable("?module=albums&function=load_artists"), {'load_artist': true});
            $.post(amigable("?module=albums&function=fwrite_artists"), {'fwrite_artists': true}, function() {
                //console.log("response");
            });
        }
    }).fail(function(response) {
        load_artist_v2(amigable("?module=resources&function=ListOfArtistsNamesByName.json"));
    });
}

function load_genre_v2() {
    $.get(amigable("?module=resources&function=genresandsubgenres.xml"), function (xml) {
        $("#genre").empty();
        $("#genre").append('<option value="" selected="selected">' + localStorage.genre + '</option>');

        $(xml).find("genre").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#genre").append("<option value='" + id + "'>" + name + "</option>");
        });
    });
}

function load_genre_v1() {
    $.post(amigable("?module=albums&function=load_genres"), {'load_genres': true}, function( response ) {
            $("#genre").empty();
            $("#genre").append('<option value="" selected="selected">' + localStorage.genre + '</option>');

            var json = JSON.parse(response);
            var genres=json.genre;
            if(genres === 'error'){
                load_genre_v2();
            }else{
                for (var i = 0; i < genres.length; i++) {
                    $("#genre").append("<option value='" + genres[i].id + "'>" + genres[i].nombre + "</option>");
                }
            }
    });
}

function load_subgenre_v2(gen) {
    $.get(amigable("?module=resources&function=genresandsubgenres.xml"), function (xml) {
        $("#subgenre").empty();
        $("#subgenre").append('<option value="" selected="selected">Select one subgenre</option>');
        $(xml).find('genre[id=' + gen + ']').each(function(){
            $(this).find('subgenre').each(function(){
                 $("#subgenre").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    });
}

function load_subgenre_v1(gen) {
    var datos = { idGenre : gen  };
    $.post(amigable("?module=albums&function=load_subgenres"), datos, function(response) {
        var json = JSON.parse(response);
        var subgenres=json.subgenre;
        $("#subgenre").empty();
        $("#subgenre").append('<option value="" selected="selected">Select one subgenre</option>');

        if(subgenres === 'error'){
            load_subgenre_v2(gen);
        }else{
            for (var i = 0; i < subgenres.length; i++) {
                $("#subgenre").append("<option value='" + subgenres[i].subgenre + "'>" + subgenres[i].subgenre + "</option>");
            }
        }
    });
}
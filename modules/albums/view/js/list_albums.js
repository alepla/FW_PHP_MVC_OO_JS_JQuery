////////////////////////////////////////////////////////////////
function load_albums_ajax() {
    $.post(amigable("?module=albums&function=load"), {'load': true}, function (data) {
        var json = JSON.parse(data);
        print_album(json);
    });
}

$(document).ready(function () {
    load_albums_ajax();
});

function print_album(data) {
    //console.log(data);
    var info = "<div class='w3_two_grid_left1 cesta'>" +
                "<div class='img_alb'><img src=" + data.album.prodpic + "></div>" +
                "<h4>Name: " + data.album.discname + "</h4>" +
                "<h4>ID: " + data.album.discid + "</h4>" +
                "<h4>Artist: " + data.album.artist + "</h4>" +
                "<p>Price: " + data.album.discprice + "</p>" +
                "<p>Date entry: " + data.album.date_entry + "</p>" +
                "<p>Date expiration: " + data.album.date_expiration + "</p>" +
                "<p>Format: " + data.album.format + "</p>" +
                "<p>Valoration: " + data.album.valoration + "</p>" +
                "<p>Description: " + data.album.discdesc + "</p>" +
                "<p>Genre: " + data.album.genre + "</p>" +
                "<p>Subgenre: " + data.album.subgenre + "</p>" +
                "<div class='clearfix'></div>" +
              "</div>";
    $("#album_info").append(info);
}

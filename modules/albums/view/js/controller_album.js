Dropzone.autoDiscover = false;
$(document).ready(function () {
    $('#submit_disc').click(function(){
        validate_disc();
    });

    $( "#date_entry" ).datepicker({
        dateFormat: 'dd/mm/yy',
        //dateFormat: 'mm-dd-yy',
        changeMonth: true, changeYear: true,
        minDate: 0, maxDate: "+1M"
    });
    $( "#date_expiration" ).datepicker({
        dateFormat: 'dd/mm/yy',
        //dateFormat: 'mm-dd-yy',
        changeMonth: true, changeYear: true,
        minDate: 30, maxDate: "+36M"
    });

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var disc_id = /^[0-9]{5}$/;
    var disc_price = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
    //var string_description = /^(.){1,500}$/;
    var string_description = /^[0-9A-Za-z]{2,90}$/;

    $("#discid").keyup(function () {
      if ($(this).val() !== "" && disc_id.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discname").keyup(function () {
      if ($(this).val() !== "" && string_reg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discprice").keyup(function () {
      if ($(this).val() !== "" && disc_price.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#discdesc").keyup(function () {
      if ($(this).val() !== "" && string_description.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#dropzone").dropzone({
        url: amigable("?module=albums&function=upload"),
        params:{'upload':true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
            });
        },
        complete: function (file) {
            /*if(file.status == "success"){
              alert("El archivo se ha subido correctamente: " + file.name);
            }*/
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: amigable("?module=albums&function=delete_pic"),
                data: {"filename":name,"delete":true},
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    } else {
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }

                }
            });
        }
    });
    load_artist_v1();
    
    $("#genre").empty();
    $("#genre").append('<option value="" selected="selected">Select genre</option>');
    $("#genre").prop('disabled', true);
    $("#subgenre").empty();
    $("#subgenre").append('<option value="" selected="selected">Select subgenre</option>');
    $("#subgenre").prop('disabled', true);

    $("#artist").change(function() {
        var artist = $(this).val();
        var genre = $("#genre");
        var subgenre = $("#subgenre");
             genre.prop('disabled', false);
             subgenre.prop('disabled', false);
             load_genre_v1();
    });

    $("#genre").change(function() {
        var gen = $(this).val();
        if(gen > 0){
            load_subgenre_v1(gen);
        }else{
            $("#subgenre").prop('disabled', false);
        }
    });

    $.post(amigable("?module=albums&function=load_data"), {'load_data': true},
      function(response){
        if(response.album===""){
            $("#discname").val('');
            $("#discid").val('');
            $("#discprice").val('');
            $("#date_entry").val('');
            $("#date_expiration").val('');
            $('#artist').val('Select artist');
            $('#genre').val('Select genre');
            $('#subgenre').val('Select subgenre');
            $("#discdesc").val('');
            var inputElements = document.getElementsByClassName('formatCheckbox');
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked){
                    inputElements[i].checked = false;
                }
            }
        }else{
          $("#discname").val(response.album.discname);
          $("#discid").val(response.album.discid);
          $("#discprice").val(response.album.discprice);
          $("#date_entry").val(response.album.date_entry);
          $("#date_expiration").val(response.album.date_expiration);
          $('#artist').val(response.album.artist);
          $('#genre').val(response.album.genre);
          $('#subgenre').val(response.album.subgenre);
          $("#discdesc").val(response.album.discdesc);
          var valo = response.album.valoration;
          var inputElements = document.getElementsByClassName('formatCheckbox');
          for (var j = 0; j < valo.length; j++) {
              for (var k = 0; k < inputElements.length; k++) {
                if (valo[j] === inputElements[k]){
                    inputElements[k].checked = true;
                }
              }
          }
        }
      }, "json");
});

function validate_disc(){
    var result = true;

    var discname = document.getElementById('discname').value;
    var discid = document.getElementById('discid').value;
    var discprice = document.getElementById('discprice').value;
    var date_entry = document.getElementById('date_entry').value;
    var date_expiration = document.getElementById('date_expiration').value;
    var format = [];
    var inputElements = document.getElementsByClassName('formatCheckbox');
    var j=0;
    for (var i=0; i< inputElements.length; i++){
        if (inputElements[i].checked){
          format[j] = inputElements[i].value;
          j++;
        }
    }
    var valoration = $('input[name="valoration"]:checked').val();

    var c = document.getElementById('artist');
    var artist = c.options[c.selectedIndex].text;
    var subgenre = document.getElementById('subgenre').value;
    var discdesc = document.getElementById('discdesc').value;

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    //var val_dates = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var disc_id = /^[0-9]{5}$/;
    ///(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/
    var prod_price = /^[0-9]{1,20}$/;
    var string_description = /^(.){1,250}$/;

    $(".error").remove();
    if ($("#discid").val() === "" || $("#discid").val() === "Input disc id") {
        $("#discid").focus().after("<span class='error'>Input disc id</span>");
        return false;
    }else if (!disc_id.test($("#discid").val())) {
        $("#discid").focus().after("<span class='error'>ID must be 5 numbers</span>");
        return false;
    }

    if ($("#discname").val() === "" || $("#discname").val() === "Input disc name"){
      $("#discname").focus().after("<span class='error'>Input disc name</span>");
      return false;
    }else if(!string_reg.test($("#discname").val())){
      $("#discname").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
      return false;
    }

    if ($("#discprice").val() === "" || $("#discprice").val() === "Input disc price") {
        $("#discprice").focus().after("<span class='error'>Input disc price</span>");
        return false;
    }else if (!prod_price.test($("#discprice").val())) {
        $("#discprice").focus().after("<span class='error'>Price must be numbers</span>");
        return false;
    }

    if ($("#date_entry").val() === "" || $("#date_entry").val() === "Input disc entry date") {
        $("#date_entry").focus().after("<span class='error'>Input disc entry date</span>");
        return false;
    }else if (!val_dates.test($("#date_entry").val())) {
        $("#date_entry").focus().after("<span class='error'>Input disc entry date</span>");
        return false;
    }

    if ($("#date_expiration").val() === "" || $("#date_expiration").val() === "Input dsic expiration date") {
        $("#date_expiration").focus().after("<span class='error'>Input dsic expiration date</span>");
        return false;
    }else if (!val_dates.test($("#date_expiration").val())) {
        $("#date_expiration").focus().after("<span class='error'>Input disc expiration date</span>");
        return false;
    }

    if ($("#discdesc").val() === "" || $("#discdesc").val() === "Input disc description") {
        $("#discdesc").focus().after("<span class='error'>Input disc description</span>");
        return false;
    }else if (!string_description.test($("#discdesc").val())) {
        $("#discdesc").focus().after("<span class='error'>Description cannot be empty</span>");
        return false;
    }

    if ($("#artist").val() === "" || $("#artist").val() === "Select artist" || $("#artist").val() === null) {
        $("#artist").focus().after("<span class='error'>Select one artist</span>");
        return false;
    }

    if ($("#genre").val() === "" || $("#genre").val() === "Select genre") {
        $("#genre").focus().after("<span class='error'>Select one genre</span>");
        return false;
    }
    var p = document.getElementById('genre');
    var genre = p.options[p.selectedIndex].text;

    if ($("#subgenre").val() === "" || $("#subgenre").val() === "Select subgenre") {
        $("#subgenre").focus().after("<span class='error'>Select one subgenre</span>");
        return false;
    }

    if (result){

      var data = {"discname": discname, "discid": discid, "discprice": discprice, "date_entry": date_entry, "date_expiration": date_expiration, "format": format, "valoration": valoration, "artist": artist, "genre": genre, "subgenre": subgenre, "discdesc": discdesc};
      var data_albums_JSON = JSON.stringify(data);

        $.post(amigable("?module=albums&function=alta_albums"), {alta_albums_json:data_albums_JSON}, function (response){
            if(response.success){
                window.location.href = response.redirect;
            }
        },"json").fail(function(xhr, textStatus, errorThrown){
            if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null ){
                xhr.responseJSON = JSON.parse(xhr.responseText);
            }

            $(".error1").remove();

            if(xhr.responseJSON.error.discname){
                $("#error_discname").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discname + "</span>");
            }

            if(xhr.responseJSON.error.discid){
                $("#error_discid").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discid + "</span>");
            }

            if(xhr.responseJSON.error.discprice){
                $("#error_discprice").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discprice + "</span>");
            }

            if(xhr.responseJSON.error.date_entry){
                $("#error_date_entry").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_entry + "</span>");
            }

            if(xhr.responseJSON.error.date_expiration){
                $("#error_date_expiration").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_expiration + "</span>");
            }

            if(xhr.responseJSON.error.format){
                $("#error_format").focus().after("<span  class='error1'>" + xhr.responseJSON.error.format + "</span>");
            }

            if(xhr.responseJSON.error.artist){
                $("#error_artist").focus().after("<span  class='error1'>" + xhr.responseJSON.error.artist + "</span>");
            }

            if(xhr.responseJSON.error.genre){
                $("#error_genre").focus().after("<span  class='error1'>" + xhr.responseJSON.error.genre + "</span>");
            }

            if(xhr.responseJSON.error.subgenre){
                $("#error_subgenre").focus().after("<span  class='error1'>" + xhr.responseJSON.error.subgenre + "</span>");
            }

            if(xhr.responseJSON.error.discdesc){
                $("#error_discdesc").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discdesc + "</span>");
            }

            if(xhr.responseJSON.error.prodpic){
                $("#prodpic").focus().after("<span  class='error1'>" + xhr.responseJSON.error.prodpic + "</span>");
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }
        });
    }
}

function load_artist_v2(cad, post_data) {
    $.post(cad, post_data, function(data) {
        var obj = JSON.parse(data);
        $("#artist").empty();
        $("#artist").append('<option value="" selected="selected">Select artist</option>');
        $.each(obj, function (i, valor) {
            for (var j = 0; j < valor.artist.length; j++) {
                $("#artist").append("<option value='" + valor.artist[j].playcount + "'>" + valor.artist[j].name + "</option>");
            }
        });
    });
}

function load_artist_v1() {
    $.post(amigable("?module=albums&function=load_artists"), {'load_artist': true}, function( response ) {
        if(response === 'error'){
            load_artist_v2(amigable("?module=resources&function=ListOfArtistsNamesByName.json"));
        }else{
            load_artist_v2(amigable("?module=albums&function=load_artists"), {'load_artist': true});
            $.post(amigable("?module=albums&function=fwrite_artists"), {'fwrite_artists': true}, function() {
                //console.log("response");
            });
        }
    }).fail(function(response) {
        load_artist_v2(amigable("?module=resources&function=ListOfArtistsNamesByName.json"));
    });
}

function load_genre_v2() {
    $.get(amigable("?module=resources&function=genresandsubgenres.xml"), function (xml) {
        $("#genre").empty();
        $("#genre").append('<option value="" selected="selected">Select genre</option>');

        $(xml).find("genre").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#genre").append("<option value='" + id + "'>" + name + "</option>");
        });
    });
}

function load_genre_v1() {
    $.post(amigable("?module=albums&function=load_genres"), {'load_genres': true}, function( response ) {
            $("#genre").empty();
            $("#genre").append('<option value="" selected="selected">Select genre</option>');

            var json = JSON.parse(response);
            var genres=json.genre;
            if(genres === 'error'){
                load_genre_v2();
            }else{
                for (var i = 0; i < genres.length; i++) {
                    $("#genre").append("<option value='" + genres[i].id + "'>" + genres[i].nombre + "</option>");
                }
            }
    });
}

function load_subgenre_v2(gen) {
    $.get(amigable("?module=resources&function=genresandsubgenres.xml"), function (xml) {
        $("#subgenre").empty();
        $("#subgenre").append('<option value="" selected="selected">Select subgenre</option>');
        $(xml).find('genre[id=' + gen + ']').each(function(){
            $(this).find('subgenre').each(function(){
                 $("#subgenre").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    });
}

function load_subgenre_v1(gen) {
    var datos = { idGenre : gen  };
    $.post(amigable("?module=albums&function=load_subgenres"), datos, function(response) {
        var json = JSON.parse(response);
        var subgenres=json.subgenre;
        $("#subgenre").empty();
        $("#subgenre").append('<option value="" selected="selected">Select subgenre</option>');

        if(subgenres === 'error'){
            load_subgenre_v2(gen);
        }else{
            for (var i = 0; i < subgenres.length; i++) {
                $("#subgenre").append("<option value='" + subgenres[i].subgenre + "'>" + subgenres[i].subgenre + "</option>");
            }
        }
    });
}
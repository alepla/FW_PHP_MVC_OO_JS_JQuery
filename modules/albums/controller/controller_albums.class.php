<?php

class controller_albums {
  function __construct() {
      include(FUNCTIONS_ALBUMS . "functions_albums.inc.php");
      include(UTILS . "upload.php");
      $_SESSION['module'] = "albums";
  }

  function list_crud() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");
      loadView('modules/albums/view/', 'list_albums.html');
      require_once(VIEW_PATH_INC . "footer.html");
  }

  function create_albums() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");
      loadView('modules/albums/view/', 'create_album.html');
      require_once(VIEW_PATH_INC . "footer.html");
  }

  function results_albums() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");
      loadView('modules/albums/view/', 'results_albums.html');
      require_once(VIEW_PATH_INC . "footer.html");
  }

  function update_albums(){
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");
      loadView('modules/albums/view/', 'update_albums.html');
      require_once(VIEW_PATH_INC . "footer.html");
  }

  function upload() {
    if ((isset($_POST["upload"])) && ($_POST["upload"] == true)){
      $result_prodpic = upload_files();
      $_SESSION['result_prodpic'] = $result_prodpic;
    }
  }

  //////////////////////////////////////////////////////////////// alta_albums
  function alta_albums(){
    if ((isset($_POST['alta_albums_json']))) {
      $jsondata = array();
      $albumsJSON = json_decode($_POST["alta_albums_json"], true);
      $result = validate_albums($albumsJSON);

      if (empty($_SESSION['result_prodpic'])){
        $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => "/workspace/FW_PHP_MVC_OO_JS_JQuery/media/default-avatar.jpg");
      }
      $result_prodpic = $_SESSION['result_prodpic'];

      if(($result['result']) && ($result_prodpic['result'])) {
          $arrArgument = array(
            'discname' => $result['data']['discname'],
            'discid' => $result['data']['discid'],
            'discprice' => $result['data']['discprice'],
            'date_entry' => $result['data']['date_entry'],
            'date_expiration' => $result['data']['date_expiration'],
            'format' => $result['data']['format'],
            'artist' => $result['data']['artist'],
            'genre' => $result['data']['genre'],
            'subgenre' => $result['data']['subgenre'],
            'valoration' => $result['data']['valoration'],
            'discdesc' => $result['data']['discdesc'],
            'prodpic' => $result_prodpic['data']
          );

          $arrValue = false;
          $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "create_album", $arrArgument);

          if ($arrValue){
              $message = "Product has been successfull registered";
          }else{
              $message = "Problem ocurred registering a porduct";
          }

          $_SESSION['album'] = $arrArgument;
          $_SESSION['message'] = $message;
          $callback="../../albums/results_albums/";

          $jsondata['success'] = true;
          $jsondata['redirect'] = $callback;
          echo json_encode($jsondata);
          exit;

      }else{
        $jsondata['success'] = false;
        $jsondata['error'] = $result['error'];
        $jsondata['error_prodpic'] = $result_prodpic['error'];

        $jsondata['success1'] = false;
        if ($result_prodpic['result']) {
            $jsondata['success1'] = true;
            $jsondata['prodpic'] = $result_prodpic['data'];
        }
        header('HTTP/1.0 400 Bad error');
        echo json_encode($jsondata);
      }
    }
  }

  //////////////////////////////////////////////////////////////// delete_pic
  function delete_pic() {
    if ((isset($_POST["delete"])) && ($_POST["delete"] == true)){
        $_SESSION['result_prodpic'] = array();
        $result = remove_files();
        if($result === true){
          echo json_encode(array("res" => true));
        }else{
          echo json_encode(array("res" => false));
        }
    }
  }

  //////////////////////////////////////////////////////////////// load
  function load() {
    if (isset($_POST["load"]) && $_POST["load"] == true) {
        $jsondata = array();
        if (isset($_SESSION['album'])) {
            $jsondata["album"] = $_SESSION['album'];
        }
        if (isset($_SESSION['message'])) {
            $jsondata["message"] = $_SESSION['message'];
        }
        close_session();
        echo json_encode($jsondata);
        exit;
    }
    function close_session() {
      unset($_SESSION['album']);
      unset($_SESSION['message']);
      $_SESSION = array();
      session_destroy();
    }
  }

  /////////////////////////////////////////////////// load_data
  function load_data() {
    if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
      $jsondata = array();
      if (isset($_SESSION['album'])) {
          $jsondata["album"] = $_SESSION['album'];
          echo json_encode($jsondata);
          exit;
      } else {
          $jsondata["album"] = "";
          echo json_encode($jsondata);
          exit;
      }
    }
  }

  /////////////////////////////////////////////////// load_artists
  function load_artists() {
    if((isset($_POST["load_artist"])) && ($_POST["load_artist"] == true)){
      $json = array();
      $url = 'http://ws.audioscrobbler.com/2.0/?method=user.gettopartists&user=rj&api_key=12ee421714031b80f353249c1a69780b&format=json';
      $json = loadModel(MODEL_ALBUMS, "albums_model", "obtain_artists", $url);
      if($json){
        echo $json;
        exit;
      }else{
        $json = "error";
        echo $json;
        exit;
      }
    }
  }

  function fwrite_artists() {
    if((isset($_POST["fwrite_artists"])) && ($_POST["fwrite_artists"] == true)){
      $json = array();
      $url = 'http://ws.audioscrobbler.com/2.0/?method=user.gettopartists&user=rj&api_key=12ee421714031b80f353249c1a69780b&format=json';
      $json = loadModel(MODEL_ALBUMS, "albums_model", "obtain_artists", $url);
      $file = fopen("resources/ListOfArtistsNamesByName.json","w");
      echo fwrite($file, $json);
      fclose($file);
      exit;
    }
  }

  /////////////////////////////////////////////////// load_genres
  function load_genres() {
    if((isset($_POST["load_genres"])) && ($_POST["load_genres"] == true)){
      $jsondata = array();
      $json = array();
      $json = loadModel(MODEL_ALBUMS, "albums_model", "obtain_genres");
      if($json){
        $jsondata["genre"] = $json;
        echo json_encode($jsondata);
        exit;
      }else{
        $jsondata["genre"] = "error";
        echo json_encode($jsondata);
        exit;
      }
    }  
  }


  /////////////////////////////////////////////////// load_subgenres
  function load_subgenres() {
    if(  isset($_POST['idGenre']) ){
      $jsondata = array();
      $json = array();
      $json = loadModel(MODEL_ALBUMS, "albums_model", "obtain_subgenres", $_POST['idGenre']);

      if($json){
        $jsondata["subgenre"] = $json;
        echo json_encode($jsondata);
        exit;
      }else{
        $jsondata["subgenre"] = "error";
        echo json_encode($jsondata);
        exit;
      }
    }
  }

  function list_albums(){
    $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "list_all_albums");

    if($arrValue){
      $jsondata = $arrValue;
      echo json_encode($jsondata);
    }else{
      $jsondata = "error";
      echo json_encode($jsondata);
    }
  }

  function read_modal(){
    $id = $_POST['read'];
    $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "read_album", $id);

    if($arrValue){
      $jsondata = $arrValue;
      echo json_encode($jsondata);
    }else {
      $jsondata = "error";
      echo json_encode($jsondata);
    }
  }

  function delete_album(){
    $id = $_POST['delete'];
    $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "delete_album", $id);

    if($arrValue){
      $jsondata = "good";
      echo json_encode($jsondata);
    }else {
      $jsondata = "error";
      echo json_encode($jsondata);
    }
  }

  function update_alta_albums(){
    $jsondata = array();
    $albumsJSON = json_decode($_POST["update_albums_json"], true);
    $avatar = $albumsJSON['avatar'];
    $result = validate_update_albums($albumsJSON);

    if (empty($_SESSION['result_prodpic'])){
      $result_prodpic = array('result' => true, 'error' => "", "data" => $avatar);
    }else{
      $result_prodpic = $_SESSION['result_prodpic'];
    }

    if(($result['result']) && ($result_prodpic['result'])) {
        $arrArgument = array(
          'discname' => $result['data']['discname'],
          'discid' => $result['data']['discid'],
          'discprice' => $result['data']['discprice'],
          'date_entry' => $result['data']['date_entry'],
          'date_expiration' => $result['data']['date_expiration'],
          'format' => $result['data']['format'],
          'artist' => $result['data']['artist'],
          'genre' => $result['data']['genre'],
          'subgenre' => $result['data']['subgenre'],
          'valoration' => $result['data']['valoration'],
          'discdesc' => $result['data']['discdesc'],
          'prodpic' => $result_prodpic['data']
        );

        $arrValue = false;
        $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "update_album", $arrArgument);

        if ($arrValue){
            session_unset($_SESSION['result_prodpic']);
            $message = "Product has been successfull registered";
        }else{
            $message = "Problem ocurred registering a porduct";
        }

        $_SESSION['album'] = $arrArgument;
        $_SESSION['message'] = $message;
        $jsondata['success'] = true;
        echo json_encode($jsondata);
        exit;

    }else{
      $jsondata['success'] = false;
      $jsondata['error'] = $result['error'];
      $jsondata['error_prodpic'] = $result_prodpic['error'];
      header('HTTP/1.0 400 Bad error');
      echo json_encode($jsondata);
    }
  }
}

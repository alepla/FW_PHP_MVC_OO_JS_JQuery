<?php

function validate_albums($value){
    $error = array();
    $valid = true;
    $filter = array(
        'discname' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z\s]{5,30}$/')
        ),
        'discid' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z]{5,30}$/')
        ),
        'discprice' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,20}$/')
        ),
        'date_entry' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'date_expiration' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'discdesc' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(.){1,250}$/')
        ),
    );

    $result = filter_var_array($value, $filter);

    $result['format'] = $value['format'];
    $result['valoration'] = $value['valoration'];
    $result['artist'] = $value['artist'];
    $result['genre'] = $value['genre'];
    $result['subgenre'] = $value['subgenre'];

    if (count($result['format']) < 1 ){
        $error['format'] = "Select 1 format or more";
        $valid = false;
    }

    if (id_rep($result['discid'])){
        $error['discid'] = "The ID can't be duplicated";
        $valid = false;
    }

    if($result['date_entry'] && $result['date_expiration']){
        $dates = validate_dates3($result['date_entry'],$result['date_expiration']);
        if($dates){
            $error['date_entry'] = 'Entry date must be before of the expiration date';
            $error['date_expiration'] = 'Expiration date must be after reception date';
            $valid = false;
        }
    }

    if($result['discname']==='Input disc name'){
            $error['discname']="Name must be 2 to 30 letters";
            $valid = false;
    }

    if ($result['artist']==='Select artist'){
            $error['artist']="You need to choose a artist";
            $valid = false;
        }

    if ($result['genre']==='Select genre'){
            $error['genre']="You need to choose a genre";
            $valid = false;
        }

    if ($result['subgenre']==='Select subgenre'){
            $error['subgenre']="You need to choose a subgenre";
            $valid = false;
        }

    if ($result['discdesc']==='Input disc description'){
            $error['discdesc']="Please enter a description";
            $valid = false;
    }

    if ($result != null && $result){
        if(!$result['discname']){
            $error['discname'] = "PHP Name must be 2 to 30 letters";
            $valid = false;
        }

        if(!$result['discid']){
            $error['discid'] = "ID must be 5 numbers";
            $valid = false;
        }

        if(!$result['discprice']){
            $error['discprice'] = "Price must be numbers";
            $valid = false;
        }

        if (!$result['date_entry']) {
            if ($result['date_entry'] == "") {
                $error['date_entry'] = "Reception date can't be empty";
                $valid = false;
            } else {
                $error['date_entry'] = 'error reception format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if (!$result['date_expiration']) {
            if ($result['date_expiration'] == "") {
                $error['date_expiration'] = "Expiration date can't be empty";
                $valid = false;
            } else {
                $error['date_expiration'] = 'error format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if(!$result['discdesc']){
            $error['discdesc'] = "Description must be 1 to 250 letters";
            $valid = false;
        }

    } else {
        $valid = false;
    };

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}


//http://stackoverflow.com/questions/8722806/how-to-compare-two-dates-in-php

//http://php.net/manual/es/datetime.diff.php

function id_rep($id){
    $arrValue = loadModel(MODEL_ALBUMS, "albums_model", "sele_id", $id);
    return $arrValue;
}

function val_dates($datetime1,$datetime2){
    $date1 = new DateTime();
    $newDate1 = $date1->createFromFormat('d/m/Y', $datetime1);
    $date2 = new DateTime();
    $newDate2 = $date2->createFromFormat('d/m/Y', $datetime2);
    var_dump($newDate1->diff($newDate2));

    if($date1 <= $date2){
      return true;
    }
      return false;
 }

function validate_dates($start_days, $dayslight) {
    $start_day = date("m/d/Y", strtotime($start_days));
    $daylight = date("m/d/Y", strtotime($dayslight));

    list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    list($mes_two, $dia_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one . "-" . $mes_one . "-" . $dia_one);
    $dateTwo = new DateTime($anio_two . "-" . $mes_two . "-" . $dia_two);

    if ($dateOne <= $dateTwo) {
        return true;
    }
    return false;
}

function validate_dates2($first_date,$second_date){
      $day1 = substr($first_date, 0, 2);
      $month1 = substr($first_date, 3, 2);
      $year1 = substr($first_date, 6, 4);
      $day2 = substr($second_date, 0, 2);
      $month2 = substr($second_date, 3, 2);
      $year2 = substr($second_date, 6, 4);

      if(strtotime($day1 . "-" . $month1 . "-" . $year1) <= strtotime($day2 . "-" . $month2 . "-" . $year2)){
        return true;
      }
        return false;
}

function validate_dates3($date1,$date2){
  $fixedDate = $date1;
  $variableDate = $date2;
  $fixedDate = implode('', array_reverse(explode('/', $fixedDate)));
  $variableDate = implode('', array_reverse(explode('/', $variableDate)));

    if ($variableDate < $fixedDate){
        return true;
    }
        return false;
}

function check_in_range($start_date, $end_date){
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);

      if ($start_ts <= $end_ts){
        return true;
      }else{
        return false;
      }
}

function validate_update_albums($value){
    $error = array();
    $valid = true;
    $filter = array(
        'discname' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z\s]{5,30}$/')
        ),
        'discid' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z]{5,30}$/')
        ),
        'discprice' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,20}$/')
        ),
        'date_entry' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'date_expiration' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'discdesc' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(.){1,250}$/')
        ),
    );

    $result = filter_var_array($value, $filter);

    $result['format'] = $value['format'];
    $result['valoration'] = $value['valoration'];
    $result['artist'] = $value['artist'];
    $result['genre'] = $value['genre'];
    $result['subgenre'] = $value['subgenre'];

    if (count($result['format']) < 1 ){
        $error['format'] = "Select 1 format or more";
        $valid = false;
    }

    if($result['date_entry'] && $result['date_expiration']){
        $dates = validate_dates3($result['date_entry'],$result['date_expiration']);
        if($dates){
            $error['date_entry'] = 'Entry date must be before of the expiration date';
            $error['date_expiration'] = 'Expiration date must be after reception date';
            $valid = false;
        }
    }

    if($result['discname']==='Input disc name'){
            $error['discname']="Name must be 2 to 30 letters";
            $valid = false;
    }

    if ($result['artist']==='Select artist'){
            $error['artist']="You need to choose a artist";
            $valid = false;
        }

    if ($result['genre']==='Select genre'){
            $error['genre']="You need to choose a genre";
            $valid = false;
        }

    if ($result['subgenre']==='Select subgenre'){
            $error['subgenre']="You need to choose a subgenre";
            $valid = false;
        }

    if ($result['discdesc']==='Input disc description'){
            $error['discdesc']="Please enter a description";
            $valid = false;
    }

    if ($result != null && $result){
        if(!$result['discname']){
            $error['discname'] = "PHP Name must be 2 to 30 letters";
            $valid = false;
        }

        if(!$result['discid']){
            $error['discid'] = "ID must be 5 numbers";
            $valid = false;
        }

        if(!$result['discprice']){
            $error['discprice'] = "Price must be numbers";
            $valid = false;
        }

        if (!$result['date_entry']) {
            if ($result['date_entry'] == "") {
                $error['date_entry'] = "Reception date can't be empty";
                $valid = false;
            } else {
                $error['date_entry'] = 'error reception format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if (!$result['date_expiration']) {
            if ($result['date_expiration'] == "") {
                $error['date_expiration'] = "Expiration date can't be empty";
                $valid = false;
            } else {
                $error['date_expiration'] = 'error format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if(!$result['discdesc']){
            $error['discdesc'] = "Description must be 1 to 250 letters";
            $valid = false;
        }

    } else {
        $valid = false;
    };

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}
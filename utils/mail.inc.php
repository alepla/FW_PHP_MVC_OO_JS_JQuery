<?php
    function send_mail($arr) {
        $html = '';
        $subject = '';
        $body = '';
        $ruta = '';
        $return = '';
        
        switch ($arr['type']) {
            case 'alta':
                $subject = 'Your registration in Pla Music';
                $ruta = "<a href='" . amigable("?module=login&function=verify&aux=" . $arr['token'], true) . "'>here</a>";
                $body = 'Thanks for join to our application<br> To finish the registration, press ' . $ruta;
                break;
    
            case 'mod':
                $subject = 'Your new password in Pla Music<br>';
                $ruta = '<a href="' . amigable("?module=login&function=activate&aux=" . $arr['token'], true) . '">here</a>';
                $body = 'To change your password press ' . $ruta;
                break;
                
            case 'contact':
                $subject = 'Yore request to Pla Music has been sent<br>';
                $ruta = '<a href=' . 'http://' . $_SERVER['HTTP_HOST'] . '/workspace/FW_PHP_MVC_OO_JS_JQuery/'. '>here</a>';
                $body = 'To visit our website, press ' . $ruta;
                break;
    
            case 'admin':
                $subject = $arr['subject'];
                $body = 'Name: ' . $arr['name']. '<br>' .
                'Mail Contact: ' . $arr['email']. '<br>' .
                'Subject: ' . $arr['subject']. '<br>' .
                'Message: ' . $arr['message'];
                break;
        }
        
        $html .= "<html>";
        $html .= "<body>";
	       $html .= "<h4>". $subject ."</h4>";
	       $html .= $body;
	       $html .= "<br><br>";
	       $html .= "<p>Send by Pla Music</p>";
		$html .= "</body>";
		$html .= "</html>";

        //set_error_handler('ErrorHandler');
        try{
            if ($arr['type'] === 'admin'){
                $address = 'aleplacambralol@gmail.com';
            }else{
                $address = $arr['email'];
            }
            $result = send_mailgun('aleplacambralol@gmail.com', $address, $subject, $html);    
        } catch (Exception $e) {
			$return = 0;
		}
		//restore_error_handler();
        return $result;
    }
    
    function send_mailgun($from, $to, $subject, $html){
        	$config = array();
        	$config['api_key'] = "key-2598633f19e26eaeb57b5bae0590a946"; //API Key
        	$config['api_url'] = "https://api.mailgun.net/v3/sandbox23338376c81f4925abb116cd4dc8dba9.mailgun.org/messages"; //API Base URL
    
        	$message = array();
        	$message['from'] = $from;
        	$message['to'] = $to;
        	$message['h:Reply-To'] = "aleplacambralol@gmail.com";
        	$message['subject'] = $subject;
        	$message['html'] = $html;
         
        	$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $config['api_url']);
        	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        	curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        	curl_setopt($ch, CURLOPT_POST, true); 
        	curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
        	$result = curl_exec($ch);
        	curl_close($ch);
        	return $result;
        }
        
<?php
function upload_files() {
    $error = "";
    $copiarFichero = false;
    $extensiones = array('jpg', 'jpeg', 'gif', 'png', 'bmp');

    if(!isset($_FILES)) {
        $error .=  'No existe $_FILES <br>';
    }
    if(!isset($_FILES['file'])) {
        $error .=  'No existe $_FILES[file] <br>';
    }

    $imagen = $_FILES['file']['tmp_name'];
    $nom_fitxer= $_FILES['file']['name'];
    $mida_fitxer=$_FILES['file']['size'];
    $tipus_fitxer=$_FILES['file']['type'];
    $error_fitxer=$_FILES['file']['error'];

    if ($error_fitxer>0) {
        switch ($error_fitxer){
            case 1: $error .=  'The file size is too heavy <br>'; break;
            case 2: $error .=  'The file size is too large <br>';break;
            case 3: $error .=  'File upload incomplete <br>';break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //if($_FILES['avatar']['error'] !== 0) { //Assignarem a l'us default-avatar
        //$error .=  'Archivo no subido correctamente <br>';
    //}

    ////////////////////////////////////////////////////////////////////////////
    if ($_FILES['file']['size'] > 65000 ){
        $error .=  "Large File Size <br>";
    }

    ////////////////////////////////////////////////////////////////////////////
    if ($_FILES['file']['name'] !== "") {
        ////////////////////////////////////////////////////////////////////////////
        @$extension = strtolower(end(explode('.', $_FILES['file']['name'])));
        if( ! in_array($extension, $extensiones)) {
            $error .=  'Only images with this extensions: ' . implode(', ', $extensiones).' <br>';
        }
        ////////////////////////////////////////////////////////////////////////////
        if (!@getimagesize($_FILES['file']['tmp_name'])){
            $error .=  "Invalid Image File... <br>";
        }
        ////////////////////////////////////////////////////////////////////////////
        list($width, $height, $type, $attr) = @getimagesize($_FILES['file']['tmp_name']);
        if ($width > 550 || $height > 550){
            $error .=   "Maximum width and height exceeded. Please upload images below 500x500 px size <br>";
        }
    }
        /*
            $image_size_info    = getimagesize($imagen); //get image size
            if($image_size_info){
                $image_width        = $image_size_info[0]; //image width
                $image_height       = $image_size_info[1]; //image height
                $image_type         = $image_size_info['mime']; //image type
            }else{
                die("Make sure image file is valid!");
            }
        */

    ////////////////////////////////////////////////////////////////////////////
    $upfile = MEDIA_PATH.$_FILES['file']['name'];
    if (is_uploaded_file($_FILES['file']['tmp_name'])){
        if (is_file($_FILES['file']['tmp_name'])) {
            $idUnico = rand();
            $nombreFichero = $idUnico."-".$_FILES['file']['name'];
            $_SESSION['nombreFichero'] = $nombreFichero;
            $copiarFichero = true;
            $upfile = MEDIA_PATH.$nombreFichero;
        }else{
                $error .=   "Invalid File...";
        }
    }

    $i=0;
    if ($error == "") {
        if ($copiarFichero) {
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $upfile)) {
                $error .= "<p>Error to save the image.</p>";
                return $return=array('result'=>false,'error'=>$error,'data'=>"");
            }
            $upfile = '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/'.$nombreFichero;
            return $return=array('result'=>true , 'error'=>$error,'data'=>$upfile);
        }
        if($_FILES['file']['error'] !== 0) {
            $upfile = '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/default-avatar.jpg';
            return $return=array('result'=>true,'error'=>$error,'data'=>$upfile);
        }
    }else{
        return $return=array('result'=>false,'error'=>$error,'data'=>"");
    }
}

function remove_files(){
	$name = $_POST['filename'];
	if(file_exists(MEDIA_PATH.$_SESSION['nombreFichero'])){
		unlink(MEDIA_PATH.$_SESSION['nombreFichero']);
		return true;
	}else{
		return false;
	}
}

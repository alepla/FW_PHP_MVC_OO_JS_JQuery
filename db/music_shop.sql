-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-05-2018 a las 03:58:54
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `music_shop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albums`
--

CREATE TABLE `albums` (
  `discname` varchar(100) DEFAULT NULL,
  `discid` int(100) NOT NULL,
  `discprice` varchar(100) DEFAULT NULL,
  `date_entry` varchar(100) DEFAULT NULL,
  `date_expiration` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `valoration` varchar(100) DEFAULT NULL,
  `discdesc` varchar(200) DEFAULT NULL,
  `artist` varchar(200) DEFAULT NULL,
  `genre` varchar(200) DEFAULT NULL,
  `subgenre` varchar(200) DEFAULT NULL,
  `discpic` varchar(200) DEFAULT NULL,
  `n_views` int(11) DEFAULT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `albums`
--

INSERT INTO `albums` (`discname`, `discid`, `discprice`, `date_entry`, `date_expiration`, `format`, `valoration`, `discdesc`, `artist`, `genre`, `subgenre`, `discpic`, `n_views`, `lat`, `lng`, `type`) VALUES
('NWA GREATEST HITS', 10000, '25', '23/03/2018', '28/04/2018', 'CD Vinyl ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Dr. Dre', 'Hip-Hop', 'Gangsta Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/27375-1198223285_f.jpg', 23, 38.821426, -0.614682, 'album'),
('WAR AND PEACE', 10001, '24', '09/05/2018', '13/06/2018', 'CD Cassete ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Buckcherry', 'Dance', 'Chillstep', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/8445-31745-450xN.jpg', 5, 38.852814, -0.380449, 'album'),
('Out of the blue', 10003, '43', '23/03/2018', '26/04/2018', 'Vinyl Mp3 Cassete ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Blue Sky Black Death', 'Rock', 'Surf', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/3303-81ZacJvsDxL._SY355_.jpg', 5, 39.188911, -0.238382, 'album'),
('I need a dollar', 10004, '21', '31/03/2018', '28/04/2018', 'CD Vinyl Mp3 ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Tom Waits', 'Rock', 'Art Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/4244-a1ab1737b92111b7426683e5075483d5--pink-floyd-album-cover.jpg', 1, 38.340240, -0.537827, 'album'),
('Good things', 10005, '32', '23/03/2018', '26/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Miles Davis', 'R&B', 'Modern Soul', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/2556-hqdefault.jpg', 2, 39.464184, -0.381821, 'album'),
('Way dont we go', 10006, '23', '23/03/2018', '26/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Dream Theater', 'Rock', 'Cock Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14777-71hCNQgkhzL._SY355_.jpg', 2, 38.994064, -1.855078, 'album'),
('Come Down', 10007, '32', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Skid Row', 'Alternative', 'Grunge', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/8702-descarga.jpg', 1, 38.483330, -0.814673, 'album'),
('Gladiator OST', 10008, '24', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 Cassete ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Fleetwood Mac', 'Soundtrack', 'Movie Soundtrack', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/22333-60756.jpg', 2, 38.267147, -0.719092, 'album'),
('DNA Problems', 10009, '26', '23/03/2018', '21/04/2018', 'CD Vinyl ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Buckcherry', 'Hip-Hop', 'Alternative Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14469-la-et-ms-kendrick-lamars-to-pimp-a-butterfly-dense-difficult-and-grandoise-20150316-399x400.jpg', 3, 37.976746, -1.128883, 'album'),
('MAAD City', 10010, '34', '23/03/2018', '21/04/2018', 'CD Cassete ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Alpha Blondy', 'Hip-Hop', 'West Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/28883-51Zzc7PUDML._SY355_.jpg', 1, 42.687363, -2.997385, 'album'),
('GREATEST HITS 2PAC', 10012, '28', '31/03/2018', '28/04/2018', 'CD Vinyl Mp3 ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'DJ Shadow', 'Hip-Hop', 'East Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18247-hqdefault.jpg', 4, 38.817936, -0.610957, 'album'),
('Gemini', 10021, '23', '27/03/2018', '26/04/2018', 'CD Vinyl ', 'Good', 'Gemini is Macklemore first album without Ryan Lewis since the release of his solo album The Language of My World. In an interview with Rolling Stone, Macklemore said of the album, Its not extremely po', 'Dire Straits', 'Hip-Hop', 'Alternative Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/854-macklemore-gemini-cd-clean-version.jpg', 11, 42.568668, -0.545454, 'album'),
('I AM THE WEST', 10023, '15', '23/03/2018', '28/04/2018', 'CD ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Snoop Dogg', 'Blues', 'Delta Blues', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/12562-hqdefault.jpg', 13, 38.820034, -0.606305, 'album'),
('The house rules', 10032, '32', '24/03/2018', '28/04/2018', 'CD ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Mark Knopfler', 'Country', 'Outlaw Country', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/24846-christian-kane-house-rules-UCB.jpg', 1, 41.993874, -4.526682, 'album'),
('Zapatillas', 10035, '23', '24/03/2018', '27/04/2018', 'CD Mp3 ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Dire Straits', 'Pop', 'Pop/Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18223-descarga.jpg', 0, 38.537830, -0.101663, 'album'),
('Cloud mine', 10036, '24', '24/03/2018', '28/04/2018', 'CD Mp3 ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Warren G', 'Electronic', 'Basseline', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/20796-32105690_350_350.jpg', 0, 39.133450, -0.471902, 'album'),
('My type', 10038, '21', '30/03/2018', '28/04/2018', 'CD Vinyl ', 'Good', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Rage Against the Machine', 'Pop', 'Britpop', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/26-51UY4lPdbwL._SX355_.jpg', 0, 39.193928, -2.160500, 'album'),
('Welcome to the jungle', 23456, '21', '23/03/2018', '25/04/2018', 'CD Vinyl ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Rage Against the Machine', 'Rock', 'Hard Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/17375-f3d22077005b4621425731c9a5a17e0e.jpg', 0, 40.412170, -3.709850, 'album'),
('Fire and the flood', 34521, '23', '28/03/2018', '21/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Mr. Big', 'Alternative', 'Indie Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/24633-2092-268x0w.jpg', 2, 39.126629, -0.525735, 'album'),
('Sing me to sleep', 65748, '43', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 ', 'Very good', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Joe Satriani', 'Electronic', 'Electronic Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14453-faded.jpg', 1, 39.033672, -0.874001, 'album'),
('Be As You Are', 90874, '25', '05/04/2018', '25/05/2018', 'CD ', 'Very good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Jimi Hendrix', 'Rock', 'Cock Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18584-13138789_10154068840956215_4844389042469885217_n.jpg', 1, 38.513424, -1.693581, 'album'),
('Recover', 90875, '20', '04/04/2018', '24/05/2018', 'CD ', 'Good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Snoop Dogg', 'Hip-Hop', 'West Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/15667-descarga.jpg', 1, 38.874420, -1.108009, 'album'),
('Who killed Matt Maeson', 90876, '14', '04/04/2018', '24/05/2018', 'CD ', 'Good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Dream Theater', 'Alternative', 'Collage Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/4139-0006465070_350.jpg', 2, 38.485050, -2.401100, 'album');

--
-- Disparadores `albums`
--
DELIMITER $$
CREATE TRIGGER `music_shop` BEFORE INSERT ON `albums` FOR EACH ROW BEGIN
	IF NEW.discprice <= 0
		THEN SET NEW.discprice = 5;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `date_birthday` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user`, `email`, `name`, `lastname`, `password`, `date_birthday`, `type`, `city`, `avatar`, `active`, `token`) VALUES
('2Uke6R5Q6mRjQnE6OD9e7PvVvD83', 'aleplacambralol@gmail.com', '', '', '', '', 'client', '', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 1, ''),
('Alepla', 'aleplacambra@gmail.com', 'Raul', 'Cambra', '$2y$10$vikKngrhGxlQlBB6aONDe.WSpiUSSo/3n.wY3ZwUXiIBF020fxRnS', '19/12/1998', 'admin', 'Valencia', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/3210-8702-descarga.jpg', 1, 'Ver4369985976cedcfb3799275fa3fa3e71'),
('DN8Q4Lo8icdzLvvcWme5RzvH04x1', 'vicplacambra@gmail.com', 'Victoria', 'Pla', '', '27/09/2003', 'client', 'Ontinyent', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/1303-854-macklemore-gemini-cd-clean-version.jpg', 1, ''),
('Jose', 'asdqwe@gmail.com', '', '', '$2y$10$ryvlasIBkcwtgvGrAj1K0u/ragnUmfgxhRGg9O4h0kInp9OrCvhiK', '', 'client', '', 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427ed41d8cd98f00b204e9800998ecf8427e?s=400&d=identicon&r=g', 0, 'Ver7754c5efc4baaa8173c1d1fc389c5213');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`discid`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
//PROJECT
define('PROJECT', '/workspace/FW_PHP_MVC_OO_JS_JQuery/');
//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/FW_PHP_MVC_OO_JS_JQuery/';
define('SITE_ROOT', $path);
//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/workspace/FW_PHP_MVC_OO_JS_JQuery/');
//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');
//JS
define('JS_PATH', SITE_PATH . 'view/js/');

//IMG
define('IMG_PATH', SITE_PATH . 'view/img/');

//log
/*define('USER_LOG_DIR', SITE_ROOT . 'log/user/Site_User_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . 'log/general/Site_General_errors.log');*/

define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . 'model/');
//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');
//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');
//resources
define('RESOURCES', SITE_ROOT . 'resources/');
//media
define('MEDIA_PATH', SITE_ROOT . 'media/');
//utils
define('UTILS', SITE_ROOT . 'utils/');

//model albums
define('FUNCTIONS_ALBUMS', SITE_ROOT . 'modules/albums/utils/');
define('MODEL_PATH_ALBUMS', SITE_ROOT . 'modules/albums/model/');
define('DAO_ALBUMS', SITE_ROOT . 'modules/albums/model/DAO/');
define('BLL_ALBUMS', SITE_ROOT . 'modules/albums/model/BLL/');
define('MODEL_ALBUMS', SITE_ROOT . 'modules/albums/model/model/');
define('ALBUMS_JS_PATH', SITE_PATH . 'modules/albums/view/js/');

//model shop
define('UTILS_SHOP', SITE_ROOT . 'modules/shop/utils/');
define('SHOP_JS_LIB_PATH', SITE_PATH . 'modules/shop/view/lib/');
define('SHOP_JS_PATH', SITE_PATH . 'modules/shop/view/js/');
define('MODEL_PATH_SHOP', SITE_ROOT . 'modules/shop/model/');
define('DAO_SHOP', SITE_ROOT . 'modules/shop/model/DAO/');
define('BLL_SHOP', SITE_ROOT . 'modules/shop/model/BLL/');
define('MODEL_SHOP', SITE_ROOT . 'modules/shop/model/model/');

//model main
define('UTILS_MAIN', SITE_ROOT . 'modules/main/utils/');
define('MAIN_JS_LIB_PATH', SITE_PATH . 'modules/main/view/lib/');
define('MAIN_JS_PATH', SITE_PATH . 'modules/main/view/js/');
define('MODEL_PATH_MAIN', SITE_ROOT . 'modules/main/model/');
define('DAO_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
define('BLL_MAIN', SITE_ROOT . 'modules/main/model/BLL/');
define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');

//model contact
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
define('MODEL_PATH_CONTACT', SITE_ROOT . 'modules/contact/model/');
define('DAO_CONTACT', SITE_ROOT . 'modules/contact/model/DAO/');
define('BLL_CONTACT', SITE_ROOT . 'modules/contact/model/BLL/');
define('MODEL_CONTACT', SITE_ROOT . 'modules/contact/model/model/');

//model map
define('MAP_JS_PATH', SITE_PATH . 'modules/map/view/js/');
define('MODEL_PATH_MAP', SITE_ROOT . 'modules/map/model/');
define('DAO_MAP', SITE_ROOT . 'modules/map/model/DAO/');
define('BLL_MAP', SITE_ROOT . 'modules/map/model/BLL/');
define('MODEL_MAP', SITE_ROOT . 'modules/map/model/model/');

//model login
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('LOGIN_JS_PATH', SITE_PATH . 'modules/login/view/js/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');

//amigables
define('URL_AMIGABLES', TRUE);
